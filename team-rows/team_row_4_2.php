				<div class="team-row">



					<div class="team-wrapper">

						<!--elina-->

						<div class="member" id="">

							<div class="inner">

								<img src="img/team/Alina_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Alina_First.jpg')"></div>

								<div class="title">
									<div class="name">ALINA BAR</div>
									<div class="position">Patent Attorney</div>
								</div>

								<div class="bg-image hover-img" style="background-image:url('img/team/Alina_Second.jpg')"></div>

							</div>


							
							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>ALINA BAR</h3>

										<div class="sub-title">

											Patent Attorney

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:alinab@cds-ip.co.il" target="_blank" rel="_nofollow">alinab@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/alina-bar-6755528a/" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>B.Sc. Physics and Astronomy, 2012, Tel Aviv University

									</p> 



	                               <strong>Experience</strong>



	                               <p>Alina has been practicing for almost a decade in leading IP firms in Israel, where she gained comprehensive experience and expertise in a vast variety of IP disciplines. She serves clients in the fields of high-tech and physics, as well as others from government institutions, academia, start-up companies, well-established companies, and private clients. Alina has extensive experience in managing IP portfolios, providing patentability assessments, drafting and prosecuting patent and industrial design applications world-wide, as well as representing foreign clients before the Israeli Patent Office.

									</p>



	                               <strong>Professional Expertise</strong>



	                               <p>IP Portfolio Management, Patents (drafting and prosecution world-wide), Patentability Studies, Industrial Designs, Trademarks</p>


	                               <strong>Technologies</strong>

	                               <p>
	                               	Artificial Intelligence, Big Data, Communication, Computer Science, Cybersecurity, Digital Printing, Machine Learning, Medical Devices, MEMS, Military Technology, Nanotechnology, Optics, Semiconductor Technology
	                               </p>


	                               <strong>Memberships</strong>



	                               <p>Israel Patent Attorneys Association

									</p>



								</div>

							</div>

						</div>

						<!--elina-->

						<!--iris munis-->

						<div class="member" id="">

							<div class="inner">

								<img src="img/team/Iris_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Iris_First.jpg')"></div>

								<div class="title">
									<div class="name">IRIS MONIS</div>
									<div class="position">CFO& Internal Auditor, Advocate, CPA ISR.</div>
								</div>

								<div class="bg-image hover-img" style="background-image:url('img/team/Iris_Second.jpg')"></div>

							</div>


							
							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>IRIS MONIS</h3>

										<div class="sub-title">

											CFO& Internal Auditor, Advocate, CPA ISR.

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:irism@cds-ip.co.il" target="_blank" rel="_nofollow">irism@cds-ip.co.il</a></br>

<!-- 									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/irene-ezratty-farhi-686a5269/" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul> -->

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>LL.M. in Commercial Law, 2008, Tel Aviv University (in collaboration with UC Berkeley)</br>
									    CPA ISR., 1996, Accountants Council, Israel Ministry of Justice<br/>
										B.A. Economics and Accounting, 1993, Tel Aviv University


									</p> 



	                               <strong>Experience</strong>



	                               <p>
	                               	Iris has nearly twenty years of experience in a variety of senior positions in financial supervision and internal auditing. She has worked in private and public enterprises, including: the Israel Ministry of Finance, the Bank of Israel, high-tech companies, financial institutions, health institutions, and international companies. Iris was a senior consultant at one of the largest accounting firms in the country, with a specialization in risk management and internal audit. In her previous position, Iris served for 15 years as the Chief Internal Auditor at one of the leading intellectual property firms in the country.

									</p>


	                               <strong>Memberships</strong>



	                               <p>Israel Bar Association

									</p>



								</div>

							</div>

						</div>

						<!--iris munis-->																			



						<!--Igor-->

						<div class="member" id="">

							<div class="inner">

								<img src="img/team/Igor_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Igor_First.jpg')"></div>

								<div class="title">
									<div class="name">IGOR LAJANSKI</div>
									<div class="position">Patent Attorney</div>
								</div>

								<div class="bg-image hover-img" style="background-image:url('img/team/Igor_Second.jpg')"></div>

							</div>


							
							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>IGOR LAJANSKI</h3>

										<div class="sub-title">

											Patent Attorney

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:igorl@cds-ip.co.il" target="_blank" rel="_nofollow">igorl@cds-ip.co.il</a></br>

<!-- 									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/irene-ezratty-farhi-686a5269/" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul> -->

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>B.Sc. Biophysics, 2016, Bar-Ilan University

									</p> 



	                               <strong>Experience</strong>



	                               <p>Igor was certified as a patent attorney in 2021. His journey in the realm of intellectual property began with an internship while finishing his degree in biophysics. Before joining Cohn, de Vries, Stadler & Co he interned in the physics department of a leading Israeli IP firm. Igor has extensive experience in dealing with diverse issues in the field of intellectual property, working with startups, medium-sized companies, and very large corporations from Israel and abroad.

									</p>



	                               <strong>Professional Expertise</strong>



	                               <p>Patents (drafting and prosecution world-wide), Patentability Studies, Freedom to Operate studies</p>





	                               <strong>Technologies</strong>



	                               <p>
	                               	Artificial Intelligence, Big Data, Communication, Computer Science, Cybersecurity, Digital Printing, Machine Learning, Medical Devices, MEMS, Military Technology, Nanotechnology, Optics, Semiconductor Technology.
									</p>



								</div>

							</div>

						</div>

						<!--Igor-->
						
					</div>



					<div class="text-wrapper">



					</div>



				</div>