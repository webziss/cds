	<header class="site-header">



		<div class="logo">

			<a href="./">

				<img src="img/logo.svg" alt="Cohn, de Vries, Stadler & Co. Intellectual Property">

			</a>

		</div>



		<nav>



			<ul>

				<li><a href="#about">ABOUT</a></li>

				<li><a href="#recognitions">RECOGNITIONS</a></li>

				<li><a href="#team">OUR TEAM</a></li>

				<li><a href="#services">OUR SERVICES</a></li>

				<li><a href="#articles">News</a></li>

				<li><a href="#contact">CONTACT</a></li>

			</ul>

			<div class="header-socials">
			<a href="https://www.linkedin.com/company/70435376/admin/" class="linkedin" target="_blenk">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17" height="15" viewBox="0 0 17 15">
				  <defs>
				    <clipPath id="clip-path">
				      <rect id="Rectangle_18" data-name="Rectangle 18" width="17" height="15" transform="translate(0 0.144)" fill="#dedede"/>
				    </clipPath>
				  </defs>
				  <g id="Group_211" data-name="Group 211" transform="translate(1.097 -0.699)">
				    <g id="Group_162" data-name="Group 162" transform="translate(-1.097 0.555)" clip-path="url(#clip-path)">
				      <path id="Path_165" data-name="Path 165" d="M145.7,131.65h0v-5.608c0-2.743-.591-4.857-3.8-4.857a3.33,3.33,0,0,0-3,1.648h-.045V121.44h-3.041v10.209h3.167v-5.055c0-1.331.252-2.618,1.9-2.618,1.624,0,1.648,1.519,1.648,2.7v4.97Z" transform="translate(-129.557 -116.411)" fill="#dedede"/>
				      <rect id="Rectangle_17" data-name="Rectangle 17" width="3.17" height="10.209" transform="translate(1.11 5.03)" fill="#dedede"/>
				      <path id="Path_166" data-name="Path 166" d="M1.836,0A1.845,1.845,0,1,0,3.672,1.836,1.837,1.837,0,0,0,1.836,0" transform="translate(0.858 -0.052)" fill="#dedede"/>
				    </g>
				  </g>
				</svg>

			</a>

			</div>

		</nav>



		<button class="mobile-btn menu-trigger" id="trigger">

			<div class="bar"></div>

		</button>



	</header>