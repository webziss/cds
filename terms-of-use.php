<!doctype html>

<html>

<head>

	<title>Cohn, de Vries, Stadler & Co. - Terms Of Use</title>

	<meta charset="UTF-8">

	<meta name="description" content="Modern and dynamic firm concentrating decades of joint experience in providing high-quality IP services.">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="stylesheet" href="https://use.typekit.net/wuz0xor.css">

	<link rel="stylesheet" href="css/swiper-bundle.min.css">

	<link rel="stylesheet" href="css/main.css?v=10">





	<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">

	<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">

	<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">

	<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">

	<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">

	<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">

	<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">

	<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">

	<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">

	<link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">

	<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">

	<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">

	<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">

	<link rel="manifest" href="/manifest.json">

	<meta name="msapplication-TileColor" content="#ffffff">

	<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">

	<meta name="theme-color" content="#ffffff">



<!-- Global site tag (gtag.js) - Google Analytics -->

	<script async src="https://www.googletagmanager.com/gtag/js?id=G-M7CMSPWE2W"></script>

	<script>

	  window.dataLayer = window.dataLayer || [];

	  function gtag(){dataLayer.push(arguments);}

	  gtag('js', new Date());

	 

	  gtag('config', 'G-M7CMSPWE2W');

	</script>

	<!--Reactflow--><script src="https://cdnflow.co/js/5379.js"></script><!--/Reactflow-->





	<!-- <script src="https://unpkg.com/swiper/swiper-bundle.js"></script> -->

	<!-- <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script> -->

</head>







<body id="top" class="simple-page">

	

	<?php include 'content-parts/header.php';?>



	<main class="main-content simple-page">

		


	
		<section class="simple-text">
			<div class="container" style="text-align: justify;">
				<h1>COHN, DE VRIES, STADLER & CO. <br/>TERMS OF USE</h1>
				</br/>
				<p>PLEASE READ THESE TERMS AND CONDITIONS CAREFULLY BEFORE USING THIS WEBSITE.</p></br/>


				<ol>
					<li>
						<h2><b><u>Terms of Use</u></b></h2>
						<ul>
							<li>
								<b><i>Binding Agreement</i></b></br/>
								<p>
									Welcome to the website of Cohn, de Vries, Stadler & Co. found at <a href="https://cds-ip.co.il">www.cds-ip.co.il</a> (the "Website").
								</p>
								<p>
									These Terms of Use (as defined below) are a legally binding agreement between Cohn, de Vries, Stadler & Co. and you.
								</p>
								<p>
									The term "<b>Cohn, de Vries, Stadler & Co.</b>", "<b>CDS</b>" "<b>us</b>", "<b>we</b>" or "<b>our</b>", refers to Cohn, de Vries, Stadler & Co., the owner of the Website. The term "you" "your" and "yours" refers to the User of the Website.
								</p>
								<p>
									These Terms of Use govern your access, viewing and use of our Website, and the use of the Website to send us a message.
								</p>
								<p>
									We are willing to license the use of the Website to you only upon the condition that you accept all the terms and conditions contained in these Terms of Use.
								</p>
								<p>
									These Terms of Use are void where prohibited by law, and the right to view the Website is revoked in such jurisdictions.
								</p>
							</li>
							<li>
								<b><i>Accepting the Terms of Use</i></b></br/>
								<p>
									These Terms of Use are effective as of the time you access, view or otherwise use the Website, and such access, viewing or use of the Website indicates your acceptance of all of the terms and conditions of these Terms of Use.
								</p>
								<p>
									You may not access, view or use the Website and may not accept these Terms of Use, if you do not agree to these Terms of Use at any time.
								</p>
							</li>
							<li>
								<b><i>Definitions</i></b></br/>
								<p>
									For the purposes hereof:<br/><br/>
									<b>"Terms of Use"</b> means these Terms of Use and the Privacy Policy of Cohn, de Vries, Stadler & Co., and all modifications thereto.<br/><br/>
									<b>"User"</b> means any person or entity accessing, viewing or using the Website
								</p>
							</li>
						</ul>
					</li>
					<li>
						<h2><b><u>Limited License</u></b></h2>
						<p>
							Cohn, de Vries, Stadler & Co. hereby grants you a limited, personal, revocable, non-exclusive, non-transferable, non-sublicensable right to access, view and use the Website, on condition that you comply with all your obligations under these Terms of Use and all modifications thereto. We grant you no other rights, implied or otherwise. 
						</p>
					</li>
					<li>
						<h2><b><u>The Website</u></b></h2>
						<ul>
							<li>
								The information contained in our Website does not constitute legal or professional advice and should not be relied on or treated as a substitute for specific advice relevant to particular circumstances.
							</li>
							<li>
								The transmission and receipt of information contained on the Website does not form or constitute an attorney-client relationship.
							</li>
							<li>
								You expressly acknowledge and agree that any access, viewing to or use of the Website, and any consequences thereof, are at your sole risk, responsibility and liability.
							</li>
							<li>
								The form and/or features of the Website may change from time to time without prior notice.
							</li>
							<li>
								In addition, we may discontinue (permanently or temporarily) operating the Website, at our sole discretion at any time, and without liability to us or notice.
							</li>
							<li>
								Any material that you upload to the Website will be considered non-proprietary. If you send us information through the Website, you agree to provide us with accurate and complete details.
							</li>
						</ul>
					</li>

					<li>
						<h2><b><u>Restrictions on Use of the Website</u></b></h2>
						<ol>
							<li>You may not do any of the following while accessing, viewing or using the Website: <br/>
								<ul>
									<li>
										Use the Website otherwise than in compliance with these Terms of Use and all applicable local, state, federal, and national, laws, statutes, ordinances, rules and regulations; 
									</li>
									<li>
										Facilitate or encourage any violations of these Terms of Use;
									</li>
									<li>
										Except as otherwise provided in these Terms of Use, copy, reproduce, print, download or save a copy, republish, display, perform, advertise, distribute, transmit, broadcast, decompile, reverse engineer, disassemble, attempt to derive the source code of, adapt, modify, create derivative works from, sell, rent, lease, loan, make available or exploit in any form or by any means all or any portion of the Website, for any purpose; <br/><br/>
										You may, however, print a copy of individual screens appearing as part of the Website solely for your personal, internal, non-commercial, or non-profit use or records, provided that any marks, logos, copyright notices, or other legends that appear on the copied screens remain on, and are not removed from the printed or stored images of any such screens;
									</li>
									<li>
										Remove or alter any copyright notices, trademark notices or other proprietary notices or identifying marks, symbols or legends included on the Website;
									</li>
									<li>
										Access, view or use the Website without authority, interfere with, damage or disrupt, any part of the Website;
									</li>
									<li>
										Use the Website to post and/or transmit any material that is unrelated to the subject matter of the Website;
									</li>
									<li>
										Use the Website to generate and/or distribute any content which is inaccurate, harmful, threatening, tortuous, abusive, causes harassment, defamatory, pornographic, vulgar, obscene, libelous, hateful, is otherwise unlawful, misleading, malicious, or discriminatory or which harms minors in any way, or which may expose us to legal action or reputational damage; 
									</li>
									<li>
										Use the Website to generate and/or distribute any content which infringes or violates any patent, trademark, trade secret, copyright or other proprietary rights of any third party or violates any laws, contributes to or encourages infringing or otherwise unlawful conduct;
									</li>
									<li>
										Use the Website for posting and/or transmitting third party copyrighted material, or material that is subject to other third party proprietary rights, unless you have a formal license or permission from the rightful owner of the material or if you are otherwise legally entitled to post and/or send the material in question; 
									</li>
									<li>
										Use the Website to post any content which you do not have a right to make available under any law or under contractual or fiduciary relationships (such as inside information, proprietary and confidential information learned or disclosed as part of employment relationships or under nondisclosure agreements), and/or which it would be unlawful for us to use or possess in connection with the operation of the Website;
									</li>
									<li>
										Use the Website for any unlawful purposes or for threatening, harassing, or for the promotion of illegal activities (including, without limitation, provide material support or resources (or to conceal or disguise the nature, location, source, or ownership of material support or resources) to any organization(s) designated by applicable law as a terrorist organization), or otherwise violate the rights of others;
									</li>
									<li>
										Use the Website for interfering with, or disrupting (or attempting to do so), the access of any person, host or network, including, without limitation, by uploading and/or sending software viruses, trojans, worms, logic bombs, keystroke loggers, spyware, adware, and/or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer software or hardware, or which is otherwise malicious or technologically harmful; overloading, flooding, spamming, mail-bombing the Website; or by scripting the creation of content in such a manner as to interfere with or create an undue burden on the Website;
									</li>
									<li>
										Use the Website for transmitting any unsolicited or unauthorized advertising or promotional material;
									</li>
									<li>
										Do anything that could disable, overburden, or impair the proper working of the Website, including, but not limited to, attacking the Website via a denial-of-service attack, or in any other way or manner. 
									</li>
								</ul>
							</li>
							<li>
								You are solely responsible and liable for, and Cohn, de Vries, Stadler & Co. has no responsibility to you or to any third party for, any breach of your obligations under these Terms of Use and for the consequences (including any loss or damage which Cohn, de Vries, Stadler & Co. may suffer) of any such breach.
							</li>
							<li>
								Any use, or attempted use, of the Website contrary to these Terms of Use is a violation of our rights. If you breach these Terms of Use, you may be subject to prosecution and damages.
							</li>
						</ol>
					</li>
					<li>
						<h2><b><u>Privacy</u></b></h2>
						<p>
							Personally-identifying information submitted to us through the Website or other information collected by us with regard to the use of the Website is governed by the Privacy Policy of Cohn, de Vries, Stadler & Co., forming an integral part of these Terms of Use. Our Privacy Policy can be found at www.cds-ip.co.il. 
						</p>
						<p>
							We do not disclose personally identifying information to third parties except in accordance with our Privacy Policy.
						</p>
						<p>
							Our Privacy Policy also includes information about the cookies that we use.
						</p>
					</li>
					<li>
						<h2><b><u>Disclaimer of Warranties</u></b></h2>
						<p>
							THE WEBSITE IS PROVIDED "AS IS" AND "AS AVAILABLE", WITH ALL FAULTS AND WITHOUT WARRANTY OF ANY KIND, AND WE HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH RESPECT THERETO, EITHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, ANY WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, TITLE, COMPLETENESS, ACCURACY, AVAILABILITY, TIMELINESS, USEFULNESS, SECURITY, RELIABILITY AND NON-INFRINGEMENT OF THIRD PARTY RIGHTS. ALL INFORMATION PROVIDED IN CONNECTION WITH THE WEBSITE RELATES TO CIRCUMSTANCES AT THE TIME OF ITS ORIGINAL PUBLICATION AND MAY NOT HAVE BEEN UPDATED TO REFLECT SUBSEQUENT DEVELOPMENTS.
						</p>
						<p>
							We do not represent or warrant against interference with your enjoyment of the Website, that your access to, or view or use of, the Website will meet your requirements or will be error-free, uninterrupted, free of viruses, corruption, attack, interference, hacking, other security intrusion or other harmful components or that defects will be corrected. 
						</p>
						<p>
							No advice or information, whether oral or written, obtained by you from us through the Website or otherwise shall create any warranty, representation or guarantee not expressly stated in these Terms of Use.
						</p>
						<p>
							Any material downloaded or otherwise obtained through the Website is accessed, viewed and used at your own discretion and risk, and you will be solely responsible for and hereby waive any and all claims and causes of action against us with respect to any such damage and/or loss you might incur thereby.
						</p>
						<p>
							We make no guaranty of confidentiality or privacy of any communication or information transmitted through the use of the Website. 
						</p>
						<p>
							Some jurisdictions do not allow the exclusion of implied warranties or limitations on applicable statutory rights of a consumer, so the above exclusion and limitations may not apply to you.
						</p>
					</li>
					<li>
						<h2><b><u>Limitation of Liability</u></b></h2>
						<p>
							TO THE EXTENT NOT PROHIBITED BY APPLICABLE LAW, IN NO EVENT SHALL COHN, DE VRIES, STADLER & CO., ITS SHAREHOLDERS, DIRECTORS, OFFICERS, EMPLOYEES AGENTS, PARTNERS AND/OR LICENSORS, BE LIABLE FOR (A) ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, PUNITIVE, EXEMPLARY, OR CONSEQUENTIAL DAMAGES WHATSOEVER, AND/OR (B) ANY DAMAGES RESULTING FROM LOSS OF PROFITS, SALES, BUSINESS OR REVENUE; LOSS OF DATA; BUSINESS INTERRUPTION OR LOSS OF USE; LOSS OF ANTICIPATED SAVINGS; LOSS OF BUSINESS OPPORTUNITY, GOODWILL OR REPUTATION; OR ANY OTHER LOSS, INJURY, CLAIM, LIABILITY, OR DAMAGE OF ANY KIND; RESULTING IN ANY WAY FROM YOUR ACCESS, VIEW TO AND/OR USE OF, INABILITY TO ACCESS, USE AND/OR RELIANCE ON, THE WEBSITE, THE CONTENT CONTAINED ON THE WEBSITE AND/OR ANY ERRORS, OMISSIONS, AND/OR ANY OTHER MATTER RELATING TO THE WEBSITE; REGARDLESS OF THE THEORY OF LIABILITY (CONTRACT, TORT OR OTHERWISE), EVEN IF COHN, DE VRIES, STADLER & CO. HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES AND EVEN IF A REMEDY SET FORTH HEREIN FAILS ITS ESSENTIAL PURPOSE. 
						</p>
						<p>
							IN NO EVENT SHALL THE TOTAL LIABILITY OF COHN, DE VRIES, STADLER & CO. OR ITS SHAREHOLDERS, DIRECTORS, OFFICERS, EMPLOYEES AGENTS, PARTNERS AND/OR LICENSORS, TO YOU FOR ALL DAMAGES (OTHER THAN AS MAY BE REQUIRED BY APPLICABLE LAW IN CASES INVOLVING PERSONAL INJURY) EXCEED THE AMOUNT OF ONE HUNDRED U.S. DOLLARS (U.S. $100.00). 
						</p>
						<p>
							We shall not be liable for any loss or damage which occurs as a result of any virus, including, without limitation, any denial-of-service attack, or other technologically harmful material that may infect your computer equipment, computer programs, data or other materials, or otherwise for any loss or damage caused to your information technology, computer programs, platform, data or other proprietary material arising in connection with your use of the Website.
						</p>
						<p>
							You are responsible for configuring your information technology, computer programs and platform in order to access, view and use our Website and for protecting these with your own anti-virus software, firewalls and any other technical measures. We give no warranties as to the compatibility of our Website with your information technology, computer programs and platform.
						</p>
						<p>
							Any cause of action by you with respect to the Website must be instituted within one (1) year after the cause of action arose.
						</p>
						<p>
							Some jurisdictions do not allow the exclusion or limitation of liability for personal injury, or of incidental or consequential damages, so the limitations above may not apply to you.
						</p>
					</li>
					<li>
						<h2><b><u>Indemnity</u></b></h2>
						<p>
							You will indemnify and hold Cohn, de Vries, Stadler & Co. and its shareholders, directors, officers, employees, agents, partners and/or licensors (collectively, the "Indemnitees"), harmless from and against all damages, losses, and expenses of any kind (including reasonable legal fees and costs), related to any demand and/or claim brought against any of the Indemnitees by any third party, due to or arising out of your access, view and/or use of the Website, any Submissions you post, your violation of these Terms of Use and/or of any law of regulation, and/or your violation of any rights of ant third party.
						</p>
					</li>
					<li>
						<h2><b><u>Ownership</u></b></h2>
						<p>
							You hereby acknowledge that the Website as a whole constitutes an original work of authorship of Cohn, de Vries, Stadler & Co., and that all rights, ownership, title and interest in and to the Website, including, but not limited to, the compilation, collection, selection, assembling, organization, coordination and arrangement of the content (whether or not such content is protected by copyright, or by any other intellectual property rights) within the Website, and the related copyrights, trademarks and all other related intellectual property rights - are and shall remain the sole and exclusive property of Cohn, de Vries, Stadler & Co
						</p>
						<p>
							All copyrights in and to the Website are owned solely and exclusively by Cohn, de Vries, Stadler & Co. (and/or by its licensors), who reserve all their rights in law and equity. 
						</p>
						<p>
							You may use the Website only in accordance with and subject to these Terms of Use.
						</p>
						<p>
							You are not granted any right and/or license, or ownership including any copyright, trademark and other intellectual property rights to the Website, and nothing contained on the Website will be construed as conferring by implication, estoppel, or otherwise right and/or license, or ownership, other than as explicitly set forth in these Terms of Use. 
						</p>
						<p>
							THE USE OF THE WEBSITE OR ANY PART THEREOF, EXCEPT FOR USE OF THE WEBSITE AS PERMITTED IN THESE TERMS OF USE, IS STRICTLY PROHIBITED AND INFRINGES ON THE INTELLECTUAL PROPERTY RIGHTS OF COHN, DE VRIES, STADLER & CO., AND MAY SUBJECT YOU TO CIVIL AND CRIMINAL PENALTIES, INCLUDING POSSIBLE MONETARY DAMAGES FOR COPYRIGHT INFRINGEMENT.
						</p>
						<p>
							You hereby acknowledge and agrees that 'Cohn, de Vries, Stadler & Co.' and other of our trademarks, trade names, service marks, graphics, logos and other brand features used in connection with the Website, are trademarks or registered trademarks of Cohn, de Vries, Stadler & Co. Nothing in these Terms of Use gives User a right to use or display such marks in any manner.
						</p>
						<p>
							Cohn, de Vries, Stadler & Co. hereby reserves all rights not expressly granted herein.
						</p>
					</li>
					<li>
						<h2><b><u>External Websites </u></b></h2>
						<p>
							The Website may enable access to certain third party web sites, including (but not limited to) social media platforms which have a link on the Website (collectively, "<b>External Websites</b>"). 
						</p>
						<p>
							We are not responsible or liable for the practices employed by External Websites linked to the Website, nor for the information or content contained therein.
						</p>
						<p>
							When you use a link to go from the Website to External Websites, these Terms of Use and our Privacy Statement is no longer in effect. 
						</p>
						<p>
							Your browsing and interaction on any External Website, is subject to that External Website's own rules and policies.
						</p>
						<p>
							WE DO NOT WARRANT OR ENDORSE ANY ADVERTISING, PRODUCTS, SERVICES OR OTHER MATERIALS ON OR AVAILABLE FROM SUCH EXTERNAL WEBSITES AND ARE NOT LIABLE FOR ANY LOSS AND/OR DAMAGE WHICH MAY BE INCURRED BY YOU OR ANY OTHER PERSON, AS A RESULT OF, AND/OR IN CONNECTION WITH, ANY RELIANCE PLACED BY YOU ON ANY OF THE ABOVE.
						</p>
						<p>
							In addition, External Websites that may be accessed from the Website are not available in all languages or in all countries. 
						</p>
						<p>
							We make no representation that such External Websites are appropriate or available for use in any particular location. To the extent you chose to access such External Websites, you do so at your own initiative and are responsible for compliance with any applicable laws, including but not limited to applicable local laws.
						</p>
						<p>
							We reserve the right, at any time and from time to time, without liability to us or notice, to change, suspend, remove, or disable your access to any External Services, and/or to impose limits on your use of, or access to, certain External Services.
						</p>
					</li>
					<li>
						<h2><b><u>Copyright Policy</u></b></h2>
						<p>
							We will respond to notices of alleged copyright infringement that comply with applicable law and are properly provided to us. If you believe that your post has been copied in a way that constitutes copyright infringement, please provide us with the following information: (i) identification of the copyrighted work claimed to have been infringed; (ii) identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit us to locate the material; (iii) your contact information, including your address, telephone number, and an email address; (iv) your statement that you have a good faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent, or the law; and (v) a statement that the information in the notification is accurate, and, under penalty of perjury, that you are authorized to act on behalf of the copyright owner, and (vi) a physical or electronic signature of the copyright owner or a person authorized to act on their behalf.
						</p>
						<p>
							We reserve the right to remove content alleged to be infringing without prior notice and at our sole discretion.
						</p>
						<p>
							If you believe that any content infringes upon your intellectual property rights, you should notify us as set out in Section 16 below.
						</p>
					</li>
					<li>
						<h2><b><u>Termination of your access to the Website</u></b></h2>
						<p>
							We reserve the right at all times to refuse and/or terminate your access to the Website, with or without notice and at our sole discretion, without thereby incurring any liability.
						</p>
						<p>
							Termination of your access to the Website as above for any reason, shall be without prejudice to any of our rights. 
						</p>
						<p>
							Provisions which by their nature would continue beyond termination (including, without limitation, the provisions of Sections 1, 4.2, 4.3, 6, 7, 8, 9, 10 through and including 19), shall survive such termination for any reason and shall continue to apply.
						</p>
						<p>
							Upon termination of these Terms of Use, you shall cease all use of the Website. 
						</p>
					</li>
					<li>
						<h2><b><u>Restrictions on Linking or Framing the Website</u></b></h2>
						<p>
							You may not, without our express prior written permission, (i) include links to the Website (or any portions thereof), (ii) display ('frame') content of the Website (or any portions thereof) within another Internet site, or (iii) archive, cache or mirror the Website (or any portions thereof).
						</p>
					</li>
					<li>
						<h2><b><u>Feedback and Content Submission</u></b></h2>
						<p>
							You agree that any ideas, remarks, comments, proposals, suggestions, recommendations, feedback, any other input and/or information that you may provide to us (collectively, "<b>Submission</b>"), is entirely voluntary, and that we shall be free to use any such Submissions if and as we shall see fit, without any obligation or compensation to you or any other person sending the Submission.
						</p>
						<p>
							We shall have exclusive ownership of all present and future existing rights to any of Submissions of every kind and nature everywhere, and you hereby irrevocably assign to us all rights therein. 
						</p>
						<p>
							We shall not be required to treat any Submission as confidential. 
						</p>
						<p>
							You acknowledge that you are responsible for whatever material you submit, and will have full responsibility for the Submission, including its legality, reliability, appropriateness, originality, and copyright. You hereby irrevocably waive and release Cohn, de Vries, Stadler & Co. and its shareholders, directors, officers, employees, agents, partners and/or licensors, from any claims and causes of action with respect to any Submission and the use or non-use thereof by Cohn, de Vries, Stadler & Co. and its shareholders, directors, officers, employees, agents, partners and/or licensors.
						</p>
					</li>
					<li>
						<h2><b><u>Governing Law and Jurisdiction</u></b></h2>
						<p>
							These Terms of Use shall be governed and construed in accordance with the laws of the State of Israel, without reference to its conflicts of laws principles. 
						</p>
						<p>
							You hereby irrevocably submit to the exclusive jurisdiction of the competent courts in Tel-Aviv, Israel, to resolve any dispute arising out of or pursuant to these Terms of Use, and you hereby consent to the exclusive jurisdiction of and venue in such courts and waive any objection as to inconvenient forum. 
						</p>
					</li>
					<li>
						<h2><b><u>Address and Notices</u></b></h2>
						<p>
							The offices of Cohn, de Vries, Stadler & Co. are located at Ziv Towers, 24 Raoul Wallenberg Street, Tel Aviv-Yafo, 6971924, Israel. 
						</p>
						<p>
							Any questions, complaints, claims or other communication to Cohn, de Vries, Stadler & Co. concerning the Website should be directed to: <a href="mailto:info@cds-ip.co.il" target="_blank">info@cds-ip.co.il</a>.
						</p>
						<p>
							Communications made to us by email will not constitute legal notice to Cohn, de Vries, Stadler & Co. and its shareholders, directors, officers, employees, agents, partners and/or licensors.
						</p>
						<p>
							We may send you notices with respect to the Website by email, regular mail, or postings on the Website, or by any other means reasonably designed to give you such notice.
						</p>
						<p>
							All communications between Cohn, de Vries, Stadler & Co. and you shall be in the English language only.
						</p>
					</li>
					<li>
						<h2><b><u>Changes to these Terms of Use</u></b></h2>
						<p>
							We may change these Terms of Use at any time and from time to time without liability to us or notice. Any such change will be effective immediately as of the date it is posted on this page. 
						</p>
						<p>
							Your continued use of the Website after the effective date of any changes to these Terms of Use, will be deemed to constitute your acceptance of any and all such changes.
						</p>
					</li>
					<li>
						<h2><b><u>Compliance with Laws</u></b></h2>
						<p>
							You agree to use the Website only for lawful purposes, and to comply with all applicable domestic and international laws, statutes, ordinances and regulations regarding your use of the Website and the content provided therein.
						</p>
					</li>
					<li>
						<h2><b><u>Miscellaneous</u></b></h2>
						<p>
							<u>Headings.</u> The headings of the Sections in these Terms of Use are for reference only and shall not be considered in the interpretation hereof. <br/><br/>
							<u>Entire Agreement.</u> These Terms of Use, together with the Privacy Policy of Cohn, de Vries, Stadler & Co., contain the complete agreement between the parties and supersedes any prior understandings, agreements or representations by or among the parties, which relate to the subject matter of these Terms of Use.<br/><br/>
							<u>Severability.</u> In the event that any provision of these Terms of Use is held to be invalid or unenforceable, that provision shall be construed, limited, modified or deleted, to the extent necessary to eliminate any invalidity or unenforceability, and the remaining provisions of these Terms of Use remain in full force and effect.<br/><br/>
							<u>Waiver.</u> No waiver on the part of Cohn, de Vries, Stadler & Co. of any right under these Terms of Use shall be effective unless in writing and signed by our duly authorized representative. No waiver on the part of Cohn, de Vries, Stadler & Co. of any past or present right arising from any breach or failure to perform shall be deemed to be a waiver of any future right arising under these Terms of Use.
						</p>
					</li>
				</ol>

				<p>Last updated: July 21, 2021</p>

			</div>
		</section>








		<?php include 'content-parts/contact-section.php';?>



		<footer style="padding: 0.3rem 0;text-align: center;font-size: 0.6rem;">

			<a href="http://overallstudio.co.il/" rel="nofollow" target="_blank">Overall design</a> | <a href="https://epicod.co.il/" rel="nofollow" target="_blank">Epicod development</a>

		</footer>		

	</main>













	<script src="js/jquery-3.5.1.min.js"></script>

	<script src="js/jquery.validate.min.js"></script>

	<script src="js/swiper-bundle.min.js"></script>

	<script src="js/main.js"></script>

<script>(function(){ var s = document.createElement('script'), e = ! document.body ? document.querySelector('head') : document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, language : 'en', position : 'left', leadColor : '#1b1c21', triggerColor : '#ffb32f', triggerRadius : '5px', triggerPositionX : 'left', triggerPositionY : 'bottom', triggerIcon : 'wheels', triggerSize : 'small', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'left', triggerPositionY : 'bottom', triggerOffsetX : 5, triggerOffsetY : 5, triggerRadius : '5px' } }); }; e.appendChild(s);}());</script>

</body>

</html>