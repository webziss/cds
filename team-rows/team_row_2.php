				<div class="team-row">



					<div class="team-wrapper">



						<!--miriam-->

						<div class="member" id="tm4">

							<div class="inner">

								<img src="img/team/Miryam_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Miryam_First.jpg')"></div>

								<div class="title">
									<div class="name">Myriam Schnur</div>
									<div class="position">Founding Partner, Patent Attorney</div>
								</div>

								<div class="bg-image hover-img" style="background-image:url('img/team/Miryam_Second.jpg')"></div>

							</div>



							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>Myriam Schnur</h3>

										<div class="sub-title">

											Founding Partner</br>

											Patent Attorney

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:Myriams@cds-ip.co.il" target="_blank" rel="_nofollow">Myriams@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/myriam-schnur-a5b9364/" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>M.Sc. in Applied Physics, 2004, Microelectronics and electro-optics department of the Hebrew University of Jerusalem</br>

									B.Sc, Physics, 2001, the Hebrew University of Jerusalem</br>

									B.Mus in the performance arts, The Rubin Academy of music and dance.

									</p> 



	                               <strong>Experience</strong>



	                               <p>Myriam has over 15 years of experience in management of the IP matters of technology-based ventures, including portfolio management and strategic counseling. She works with start-up companies, university technology transfer companies, large corporations and multi-international companies. She provides infringement opinions, due diligence, patents invalidation and oppositions, as well as opinions regarding patentability issues, search strategies, search results analysis, or FTO evaluations. Myriam also has extensive experience in drafting and prosecuting patent applications in a vast range of physics areas. </p>



	                               <strong>Professional Expertise</strong>



	                               <p>IP Strategic Counseling, Patents, IP Due Diligence.</p>



	                               <strong>Technologies</strong>



	                               <p>Medical Devices, Optics, Semiconductors, Energy production, Nanotechnology, Imaging, Water Technologies</p>



	                               <strong>Memberships</strong>



	                               <p>Israel Patent Attorneys Association, AIPPI</p>



								</div>

							</div>

						</div>

						<!--miriam-->



						<!--idit-->

						<div class="member" id="tm5">

							<div class="inner">

								<img src="img/team/Idit_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Idit_First_New.jpg')"></div>

								<div class="title">
									<div class="name">Edith Sokol</div>
									<div class="position">Founding Partner, Patent Attorney</div>
								</div>

								<div class="bg-image hover-img" style="background-image:url('img/team/Idit_Second_New.jpg')"></div>

							</div>



							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>Edith Sokol</h3>

										<div class="sub-title">

											Founding Partner</br>

											Patent Attorney

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:Ediths@cds-ip.co.il" target="_blank" rel="_nofollow">Ediths@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/edith-sokol-194b56136" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>MBA, 2010, Technion</br>

									  M.Sc. Chemical Engineering, 2008, Technion</br>

									  B.Sc. Materials engineering & B.A. Chemistry, 2002, Technion



									</p> 



	                               <strong>Experience</strong>



	                               <p>Edith is a patent attorney with vast experience in strategic IP counseling and IP portfolio-building and management in all fields of materials science and chemistry, pharmaceuticals, polymer science, nanotechnology, chemical engineering and process development, water technologies, medical devices, and more. Her clients span over a broad range of companies, starting from local start-ups and up to international companies.</p>

	                               <p>

									Before becoming a patent attorney, Edith was a senior R&D chemical and materials engineer in the industry.

									</p>



	                               <strong>Professional Expertise</strong>



	                               <p>IP Strategic Counseling, Patents, IP Portfolio Management, IP Due Diligence</p>



	                               <strong>Technologies</strong>



	                               <p>Chemistry, Material science and engineering, Life science & Healthcare, Chemical engineering, Medical Devices, Packaging (PackTech), Water Technologies, Cleantech & Renewables, Construction & Engineering, Consumer Products, Foodtech, Agritech</p>



	                               <strong>Memberships</strong>



	                               <p>Israel Patent Attorneys Association</p>



								</div>

							</div>

						</div>

						<!--idit-->



						<!--inna-->

						<div class="member" id="tm6">

							<div class="inner">

								<img src="img/team/Ina_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Ina_First.jpg')"></div>

								<div class="title">
									<div class="name">Inna Novikova</div>
									<div class="position">Founding Partner, Patent Attorney</div>
								</div>

								<div class="bg-image hover-img" style="background-image:url('img/team/Ina_Second.jpg')"></div>

							</div>



							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>Inna Novikova</h3>

										<div class="sub-title">

											Founding Partner</br>

											Patent Attorney

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:Innan@cds-ip.co.il" target="_blank" rel="_nofollow">Innan@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/inna-novikova-34989/" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>Central College of Patent Studies, 1987, St. Petersburg</br>

									  M. Sc. Technical Physics, 1981, St. Petersburg Polytechnical University

									</p> 



	                               <strong>Experience</strong>



	                               <p>Inna is a highly experienced, value-adding patent attorney specializing in patent-related services for high-tech clients. She is a former business executive at ECI, Seabridge, Starhome, and Xpert Technologies, a former partner at a leading patent attorneys firm. With an impressive track record and industry experience, Inna brings to high-tech clients superb IP strategic counseling, invention harvesting, and patent portfolio building and managing.

									</p>



	                               <strong>Professional Expertise</strong>



	                               <p>IP Strategic Counseling, IP Audit, IP Due Diligence, IP Intelligence, M&A Advisory, Patents</p>



	                               <strong>Technologies</strong>



	                               <p>Computer-related inventions, including communication, storage, cybersecurity, Artificial Intelligence (AI) and Machine Learning (ML), Big Data, Digital Health.</p>



	                               <strong>Memberships</strong>



	                               <p>AIPPI, Israel Patent Attorneys Association</p>



								</div>

							</div>

						</div>

						<!--inna-->

																								

					</div>



					<div class="text-wrapper">



					</div>



				</div>