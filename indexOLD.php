<!doctype html>

<html>

<head>

	<title>Cohn, de Vries, Stadler & Co. - WHERE IP ASSETS MEET HUMAN VALUES</title>

	<meta charset="UTF-8">

	<meta name="description" content="Modern and dynamic firm concentrating decades of joint experience in providing high-quality IP services.">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="stylesheet" href="https://use.typekit.net/wuz0xor.css">

	<link rel="stylesheet" href="css/swiper-bundle.min.css">

	<link rel="stylesheet" href="css/main.css?v=4">





	<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">

	<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">

	<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">

	<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">

	<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">

	<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">

	<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">

	<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">

	<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">

	<link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">

	<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">

	<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">

	<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">

	<link rel="manifest" href="/manifest.json">

	<meta name="msapplication-TileColor" content="#ffffff">

	<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">

	<meta name="theme-color" content="#ffffff">



<!-- Global site tag (gtag.js) - Google Analytics -->

	<script async src="https://www.googletagmanager.com/gtag/js?id=G-M7CMSPWE2W"></script>

	<script>

	  window.dataLayer = window.dataLayer || [];

	  function gtag(){dataLayer.push(arguments);}

	  gtag('js', new Date());

	 

	  gtag('config', 'G-M7CMSPWE2W');

	</script>



	<!-- <script src="https://unpkg.com/swiper/swiper-bundle.js"></script> -->

	<!-- <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script> -->

</head>







<body id="top">

	

	<header class="site-header">



		<div class="logo">

			<a href="#top">

				<img src="img/logo.svg" alt="Cohn, de Vries, Stadler & Co. Intellectual Property">

			</a>

		</div>



		<nav>

			<ul>

				<li><a href="#about">ABOUT</a></li>

				<li><a href="#team">OUR TEAM</a></li>

				<li><a href="#services">OUR SERVICES</a></li>

				<li><a href="#articles">From the media</a></li>

				<li><a href="#contact">CONTACT</a></li>

			</ul>

		</nav>



		<button class="mobile-btn menu-trigger" id="trigger">

			<div class="bar"></div>

		</button>



	</header>



	<section class="top-banner" id="top-banner">

		<!-- <div class="container"> -->

			<div class="animation-wrapper">

				<div class="video-wrapper desktop">

					<video controls="false" autoplay loop muted>

					  <source src="img/desktop-video2.mp4" type="video/mp4">

					  <source src="img/desktop-video.ogg" type="video/ogg">

					  Your browser does not support the video tag.

					</video>

				</div>



				<div class="image mobile">

					<img src="img/mobile-animation3.gif" alt="">

				</div>

			</div>

		<!-- </div> -->

	</section>





	<main class="main-content">

		<!--about-->

		<section class="about" id="about">

			<div class="container">

				<div class="parts-wrapper">

					<div class="left-part">

						<h2 class="section-title">About us</h2>



						<h1>

							CDS - <br/>

	                        WHERE IP </br>

	                        ASSETS MEET </br>

	                        HUMAN VALUES

						</h1>

					</div>

					<div class="right-part">

						<p>

							Cohn, de Vries, Stadler & Co. is a modern and dynamic firm concentrating decades of joint experience in providing high-quality IP services. Our team of experts brings together IP expertise, technical knowledge, and business vision across a wide range of markets and technologies.

						</p>

						<p>

							Our holistic approach to IP services is tailored into your business strategy, uniqueness, competitive landscape and organizational structure to maximize your goals. We proactively assist in identifying your IP needs and adjusting the IP strategy to maximize value of your intellectual assets.

						</p>

						<p>

							We form strong partnerships with our clients, and through a flexible and creative approach to intellectual property, enable them to achieve goals, grow their business, protect and profit from their intellectual property assets, and strategically position themselves to best meet future challenges.

						</p>

						<p>

							<strong>

								Your long-lasting success is our passion 

							</strong>

						</p>

					</div>

				</div>

			</div>

		</section>

		<!--about-->



		<!--team-->

		<section class="team" id="team">

			<div class="container">

				<header class="section-header">

					<h2 class="section-title">

						Our Team

					</h2>

				</header>

<!-- 				<div class="main-sub-title">
					Professional Staff
				</div> -->



				<?php include 'team-rows/team_row_1.php';?>

				<?php include 'team-rows/team_row_2.php';?>

				<?php include 'team-rows/team_row_3.php';?>

				<?php include 'team-rows/team_row_4.php';?>





			</div>

		</section>

		<!--team-->



		<!--services-->

		<section class="services" id="services">

			<div class="container">

				<header class="section-header">

					<h2 class="section-title">

						Our Services

					</h2>

				</header>



				<div class="parts-wrapper">

					<div class="left-part">

						<p>

                          Cohn, de Vries, Stadler & Co. is a one stop professional service provider to knowledge and IP-based enterprises for building and growing value and revenue-generating IP assets. We combine many decades of expertise of leading IP professionals from a very wide range of IP disciplines to offer a full gamut of IP services, from in-depth analysis of a patent landscape, expert advice on how to better protect knowledge and ideas through to the build-up of high tier value-generating portfolios of patents and other IP assets.

						</p>

					</div>



					<div class="right-part">

						<p>The professional services we provide, include, among others</p>

						<ul class="accordion" id="accordion">



							<li>

								<div class="title">Strategic IP related</div>

								<div class="text">

									<p>

										including devising and implementing IP strategy, IP due diligence and audit, freedom-to-operate (FTO) and patentability studies, consultancy on standard-essential patents, inventions’ harvesting, advisory on M&A and licensing-related IP services

									</p>

								</div>

							</li>		



							<li>

								<div class="title">Patent related</div>

								<div class="text">

									<p>including drafting and prosecuting patent applications, management of patent portfolio, prior art and FTO searches and opinions.</p>

								</div>

							</li>


							<li>

								<div class="title">Designs</div>

								<div class="text">

									<p>

										including managing international designs portfolio, prior art searches, filing and prosecution ,FTO searches and opinions.

									</p>

								</div>

							</li>	


							<li>

								<div class="title">Trademark related</div>

								<div class="text">

									<p>including assistance in branding, trademark clearance searches, filing and prosecuting trademark applications</p>

								</div>

							</li>

							

							<li>

								<div class="title no-arrow">IP litigation support</div>

								<div class="text"></div>

							</li>

							

							<li>

								<div class="title">IP monetization</div>

								<div class="text">

									<p>

										including sale and licensing of patent and other IP assets and enforcement of IP rights

									</p>

								</div>

							</li>


						</ul>

					</div>

				</div>



			</div>

		</section>

		<!--services-->

		<!--articles-->
		<section class="articles" id="articles">
			<div class="container">

				<header class="section-header">

					<h2 class="section-title">

						From the media

					</h2>

				</header>

				<div class="articles-wrapper">
						<!-- Slider main container -->
						<div class="swiper-container">
						    <!-- Additional required wrapper -->
						    <div class="swiper-wrapper">
						        <!-- Slides -->


					        	<!--MAARIV-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				Maariv
						        				<time>December 28, 2020</time>
						        			</div>
						        			<h3 class="article-title">
						        				Three senior patent attorneys are setting up a new firm
						        			</h3>

						        			<div class="article-body">
						        				<p>
													A major change in the field of intellectual property in Israel: Three senior partners from the Reinhold Cohn Group, decided to separate and establish a new firm called Cohn, de Vries, Stadler & Co. "We intend to be a significant factor in intellectual property”.
						        				</p>
						        			</div>

						        			<a href="https://www.maariv.co.il/business/economic/israel/Article-811384" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--MAARIV-->


						        <!--JERUSALEM POST-->
						        <div class="swiper-slide">
						        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				The Jerusalem Post
						        				<time>December 30, 2020</time>
						        			</div>
						        			<h3 class="article-title">
						        				Three Senior Patent Attorneys Establish a New Firm:
						        			</h3>

						        			<div class="article-body">
						        				<p>
						        					“Intend to be a Significant Player in the Israeli IP Scene”
						        				</p>
						        			</div>

						        			<a href="https://www.jpost.com/special-content/three-senior-patent-attorneys-establish-a-new-firm-653515" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
						        </div>
						        <!--JERUSALEM POST-->

					        	<!--FORBES-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				Forbes
						        				<time>January 4, 2020</time>
						        			</div>
						        			<h3 class="article-title">
						        				IP-Key Asset of the Modern Economy: What is to be Expected Following the Covid-19 Pandemic (By Ilan Cohn)
						        			</h3>

						        			<div class="article-body">
						        				<p>
						        				Intellectual property (IP) is a key economic driver of the modern economy and is changing its role from a tool for maintaining exclusivity, to one that supports collaboration between stakeholders, each sharing their IP for a common goal.
						        				</p>
						        			</div>

						        			<a href="https://forbes.co.il/e/ip-key-asset-of-the-modern-economy-what-is-to-be-expected-following-the-covid-19-pandemic" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--FORBES-->

					        	<!--Calcalist-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				Calcalist
						        				<time>October 11, 2020</time>
						        			</div>
						        			<h3 class="article-title">
						        				An earthquake in Reinhold Cohn:</br> 3 executives retire to new office
						        			</h3>

						        			<div class="article-body">
						        				<p>
						        				Three patent attorneys, including the grandson of the firm's founder, are retiring from Israel’s leading intellectual property firm due to "ideological differences." More retirees from the firm are expected to join the three.
						        				</p>
						        			</div>

						        			<a href="https://www.calcalist.co.il/local/articles/0,7340,L-3870711,00.html" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--Calcalist-->


					        	<!--WALLA-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				Walla
						        				<time>December 28, 2020</time>
						        			</div>
						        			<h3 class="article-title">
						        				Patented: three partners, one intellectual property firm
						        			</h3>

						        			<div class="article-body">
						        				<p>
													Dr. Ilan Cohn, David de Vries and Svetlana Stadler have set up new practice with a staff of 25 employees, that handles thousands of patent, design, and trademark cases: "We intend to be a significant factor in the field of intellectual property in Israel”.
						        				</p>
						        			</div>

						        			<a href="https://finance.walla.co.il/item/3408001" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--WALLA-->

						     
						    </div>
						</div>	
			    <!-- If we need navigation buttons -->
			    <div class="swiper-button-prev">
					<svg xmlns="http://www.w3.org/2000/svg" width="32" height="60" viewBox="0 0 32 60"><g><g><path fill="none" stroke="#707070" stroke-miterlimit="20" d="M30.713 59.427v0L1 29.714v0L30.713 0v0"/></g></g></svg>
			    </div>
			    <div class="swiper-button-next">
					<svg xmlns="http://www.w3.org/2000/svg" width="32" height="60" viewBox="0 0 32 60"><g><g><path fill="none" stroke="#707070" stroke-miterlimit="20" d="M1 .001v0l29.713 29.713v0L1 59.428v0"/></g></g></svg>
			    </div>	


			    <div class="swiper-pagination"></div>

				</div>

						
			</div>
		</section>

		<!--articles-->



		<!--contact-->

		<section class="contact" id="contact">

			<div class="container">

				<div class="parts-wrapper">

					<div class="left-part">



						<header class="section-header">

							<h2 class="section-title">

								Contact

							</h2>

							<div class="small-title">
								Cohn, de Vries, Stadler & Co.
							</div>

						</header>


						<div class="contact-details">

							<div class="detail-col">
								Tel. <a href="tel:+972733989000">+972.73.3989000</a></br>
								Fax. +972.73.3989049</br>
								<a href="mailto:info@cds-ip.co.il " target="_blank">info@cds-ip.co.il</a>
							</div>
							<div class="detail-col">
								Ziv Towers, Tower D, </br>
								24 Raoul Wallenberg St.</br>
								Tel Aviv 6971924, Israel
							</div>
							<div class="detail-col">
								Mailing address:</br>
								P.O.B Box 13216</br>
								Tel Aviv 6113102, Israel
							</div>

						</div>



					</div>



					<div class="right-part">

						

						<form action="" id="main-form">

							<fieldset>

								<input type="text" name="fullname" id="fullname" placeholder="Full Name">

							</fieldset>							

							<fieldset>

								<input type="text" name="company" id="company" placeholder="Company">

							</fieldset>							

							<fieldset>

								<input type="tel" name="mobile" id="mobile" placeholder="Mobile*">

							</fieldset>							

							<fieldset>

								<input type="email" name="email" id="email" placeholder="Email">

							</fieldset>							

							<fieldset>

								<input type="submit" value="Send" >

							</fieldset>



							<div class="thankyou-message">



							</div>

						</form>

					</div>

				</div>

			</div>

		</section>

		<!--contact-->



		<footer style="padding: 0.3rem 0;text-align: center;font-size: 0.6rem;">

			<a href="http://overallstudio.co.il/" rel="nofollow" target="_blank">Overall design</a> | <a href="https://epicod.co.il/" rel="nofollow" target="_blank">Epicod development</a>

		</footer>		

	</main>













	<script src="js/jquery-3.5.1.min.js"></script>

	<script src="js/jquery.validate.min.js"></script>

	<script src="js/swiper-bundle.min.js"></script>

	<script src="js/main.js"></script>	

</body>

</html>