	<!--contact-->

		<section class="contact" id="contact">

			<div class="container">

				<div class="parts-wrapper">

					<div class="left-part">



						<header class="section-header">

							<h2 class="section-title">

								Contact

							</h2>

							<div class="small-title">
								Cohn, de Vries, Stadler & Co.
							</div>

						</header>


						<div class="contact-details">

							<div class="detail-col">
								Tel. <a href="tel:+972733989000">+972.73.3989000</a></br>
								Fax. +972.73.3989049</br>
								<a href="mailto:info@cds-ip.co.il " target="_blank">info@cds-ip.co.il</a>
							</div>
							<div class="detail-col">
								Ziv Towers, Tower D, </br>
								24 Raoul Wallenberg St.</br>
								Tel Aviv 6971924, Israel
							</div>
							<div class="detail-col">
								Mailing address:</br>
								P.O.B 13216</br>
								Tel Aviv 6113102, Israel
							</div>

						</div>





					</div>



					<div class="right-part">

						

						<form action="" id="main-form">

							<fieldset>

								<input type="text" name="fullname" id="fullname" placeholder="Full Name">

							</fieldset>							

							<fieldset>

								<input type="text" name="company" id="company" placeholder="Company">

							</fieldset>							

							<fieldset>

								<input type="tel" name="mobile" id="mobile" placeholder="Mobile*">

							</fieldset>							

							<fieldset>

								<input type="email" name="email" id="email" placeholder="Email">

							</fieldset>							

							<fieldset>

								<input type="submit" value="Send" >

							</fieldset>



							<div class="thankyou-message">



							</div>

						</form>

					</div>

				</div>



				<div class="bottom-links">
					<ul>
						<li>
							<a href="./privacy-policy.php" target="_blank">Privacy policy</a>
						</li>								
						<li>
							<a href="./terms-of-use.php" target="_blank">Term of Use</a>
						</li>								
						<li class="open-rights-text">
							Copyright © by Cohn, de Vries, Stadler & Co. 2020
						</li>
					</ul>
				</div>

				<div class="small-text rights-text">
					All Rights Reserved. Reproduction or distribution of this website and/or of any materials
					included thereon, other than for intended purposes is prohibited, without the express prior
					written permission of Cohn, de Vries, Stadler &amp; Co. The trademarks and service marks of
					Cohn, de Vries, Stadler &amp; Co., including the Cohn, de Vries, Stadler &amp; Co. mark and logo, are
					the exclusive property of Cohn, de Vries, Stadler &amp; Co., and may not be used without the
					express prior written permission of Cohn, de Vries, Stadler &amp; Co. All other trademarks
					mentioned in this website are the property of their respective owners.
				</div>


			</div>

		</section>

		<!--contact-->