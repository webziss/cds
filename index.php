<!doctype html>

<html>

<head>

	<title>Cohn, de Vries, Stadler & Co. - WHERE IP ASSETS MEET HUMAN VALUES</title>

	<meta charset="UTF-8">

	<meta name="description" content="Modern and dynamic firm concentrating decades of joint experience in providing high-quality IP services.">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="stylesheet" href="https://use.typekit.net/wuz0xor.css">

	<link rel="stylesheet" href="css/swiper-bundle.min.css">

	<link rel="stylesheet" href="css/cookie.css">

	<link rel="stylesheet" href="css/main.css?v=10">





	<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">

	<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">

	<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">

	<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">

	<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">

	<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">

	<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">

	<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">

	<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">

	<link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">

	<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">

	<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">

	<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">

	<link rel="manifest" href="/manifest.json">

	<meta name="msapplication-TileColor" content="#ffffff">

	<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">

	<meta name="theme-color" content="#ffffff">



<!-- Global site tag (gtag.js) - Google Analytics -->

	<script async src="https://www.googletagmanager.com/gtag/js?id=G-M7CMSPWE2W"></script>

	<script>

	  window.dataLayer = window.dataLayer || [];

	  function gtag(){dataLayer.push(arguments);}

	  gtag('js', new Date());

	 

	  gtag('config', 'G-M7CMSPWE2W');

	</script>

	<!--Reactflow--><script src="https://cdnflow.co/js/5379.js"></script><!--/Reactflow-->





	<!-- <script src="https://unpkg.com/swiper/swiper-bundle.js"></script> -->

	<!-- <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script> -->

</head>







<body id="top">

	

	<?php include 'content-parts/header.php';?>



	<section class="top-banner" id="top-banner">

		<!-- <div class="container"> -->

			<div class="animation-wrapper">

				<div class="video-wrapper desktop">

					<video controls="false" autoplay loop muted>

					  <source src="img/desktop-video2.mp4" type="video/mp4">

					  <source src="img/desktop-video.ogg" type="video/ogg">

					  Your browser does not support the video tag.

					</video>

				</div>



				<div class="image mobile">

					<img src="img/mobile-animation3.gif" alt="">

				</div>

			</div>

		<!-- </div> -->

	</section>





	<main class="main-content">

		<!--about-->

		<section class="about" id="about">

			<div class="container">

				<div class="parts-wrapper">

					<div class="left-part">

						<h2 class="section-title">About us</h2>



						<h1>

							CDS - <br/>

	                        WHERE IP </br>

	                        ASSETS MEET </br>

	                        HUMAN VALUES

						</h1>

					</div>

					<div class="right-part">

						<p>

							Cohn, de Vries, Stadler & Co. is a modern and dynamic firm concentrating decades of joint experience in providing high-quality IP services. Our team of experts brings together IP expertise, technical knowledge, and business vision across a wide range of markets and technologies.

						</p>

						<p>

							Our holistic approach to IP services is tailored into your business strategy, uniqueness, competitive landscape and organizational structure to maximize your goals. We proactively assist in identifying your IP needs and adjusting the IP strategy to maximize value of your intellectual assets.

						</p>

						<p>

							We form strong partnerships with our clients, and through a flexible and creative approach to intellectual property, enable them to achieve goals, grow their business, protect and profit from their intellectual property assets, and strategically position themselves to best meet future challenges.

						</p>

						<p>

							<strong>

								Your long-lasting success is our passion 

							</strong>

						</p>

					</div>

				</div>

			</div>

		</section>

		<!--about-->


		<!--recognitions-->
		<section class="recognitions" id="recognitions">
			<div class="container">
				<header class="section-header">

					<h2 class="section-title">

						Ranking & Recognition

					</h2>

				</header>		


<div class="recognition-wrapper">
	

					<!-- Slider main container -->
					<div class="swiper-container">




					    <!-- Additional required wrapper -->
					    <div class="swiper-wrapper">
					        <!-- Slides -->


				        	<!--IAM Patent 1000-->
				        	<div class="swiper-slide recognition-column">
								<div class="image">
									<a href="https://www.iam-media.com/directories/patent1000/rankings/israel" target="_blank"><img src="img/patent.png" alt=""></a>
								</div>
								<div class="text sliding-text">
									<h4><a href="https://www.iam-media.com/directories/patent1000/rankings/israel" target="_blank">IAM PATENT 1000</a></h4>
									<p>
										Cohn, de Vries Stadler & Co. ranked as <a href="https://www.iam-media.com/directories/patent1000/rankings/israel" target="_blank">"recommended firm 2021"</a> in the prosecution category</br>
										David de Vries ranked as <a href="https://www.iam-media.com/directories/patent1000/individuals/david-de-vries" target="_blank">recommended individual 2021"</a></br>
										Ilan Cohn ranked as <a href="https://www.iam-media.com/directories/patent1000/individuals/ilan-cohn" target="_blank">recommended individual 2021"</a></br>
										Svetlana Stadler ranked as <a href="https://www.iam-media.com/directories/patent1000/individuals/svetlana-stadler" target="_blank">recommended individual 2021"</a>

									</p>
									<p>
										<b>IAM SAYS</b> :"In a significant development for the market, December 2020 saw the arrival of a new force in Israeli patent practice with the establishment of Cohn, de Vries, Stadler & Co. Formed by ex-Reinhold Cohn practitioners <a href="https://www.iam-media.com/directories/patent1000/individuals/ilan-cohn" target="_blank">Ilan Cohn</a>, <a href="https://www.iam-media.com/directories/patent1000/individuals/david-de-vries" target="_blank">David de Vries</a> and <a href="https://www.iam-media.com/directories/patent1000/individuals/svetlana-stadler" target="_blank">Svetlana Stadler</a>, the boutique has been built from the ground up to provide a business-focused, agile service to innovators. Cohn is a keen strategist with a flair for helping entrepreneurs and companies to get the most out of their intellectual assets. Trained as a mechanical engineer, de Vries knows how to procure ironclad patent rights and build valuable portfolios, and is also a sought-after opinion giver. Stadler is a top-notch physicist with an instant recall of the finest details of her matters."
									</p>
								</div>
				        	</div>
				        	<!--IAM Patent 1000-->


				        	<!--duns-->
				        	<div class="swiper-slide recognition-column">
								<div class="image">
									<a href="https://www.duns100.co.il/rating/%D7%A2%D7%95%D7%A8%D7%9B%D7%99_%D7%93%D7%99%D7%9F/%D7%A7%D7%A0%D7%99%D7%99%D7%9F_%D7%A8%D7%95%D7%97%D7%A0%D7%99_Patent_Prosecution" target="_blank"><img src="img/duns2.png" alt=""></a>
								</div>
								<div class="text sliding-text">
									<h4><a href="https://www.duns100.co.il/rating/%D7%A2%D7%95%D7%A8%D7%9B%D7%99_%D7%93%D7%99%D7%9F/%D7%A7%D7%A0%D7%99%D7%99%D7%9F_%D7%A8%D7%95%D7%97%D7%A0%D7%99_Patent_Prosecution" target="_blank">Dun & Bradstreet</a></h4>
									<p>
										In 2021, following our first submission as a firm, the renowned Israeli ranking guide <b><a href="https://www.duns100.co.il/rating/%D7%A2%D7%95%D7%A8%D7%9B%D7%99_%D7%93%D7%99%D7%9F/%D7%A7%D7%A0%D7%99%D7%99%D7%9F_%D7%A8%D7%95%D7%97%D7%A0%D7%99_Patent_Prosecution" target="_blank">Dun & Bradstreet ranked Cohn, de Vries, Stadler & Co.</a></b> as a leading firm in the Patent Prosecution practice area in Israel.	
									</p>
								</div>
				        	</div>
				        	<!--duns-->

				        	<!--legal-->
				        	<div class="swiper-slide recognition-column">
								<div class="image">
									<a href="https://www.legal500.com/c/israel/intellectual-property/intellectual-property-filingprosecution/" target="_blank">
										<img src="img/legal500.png" alt="">
									</a>
								</div>
								<div class="text sliding-text">
									<h4><a href="https://www.legal500.com/c/israel/intellectual-property/intellectual-property-filingprosecution/" target="_blank">The Legal 500</a></h4>
									<p>
										The prestigious guide <b><a href="https://www.legal500.com/c/israel/intellectual-property/intellectual-property-filingprosecution/" target="_blank">The Legal 500 ranked our firm</a></b> in Tier 2 of its Patent: Filing and prosecution practice area, after only one year in operations. 
									</p>	
									<p>
										<b><a href="https://www.legal500.com/c/israel/intellectual-property/intellectual-property-filingprosecution/" target="_blank">The Legal 500 also selected Dr. Ilan Cohn</a></b> as Leading Individual in the area of Patent: Filing and prosecution
									</p>	
								</div>
				        	</div>
				        	<!--legal-->

				        	<!--chambers-->
				        	<div class="swiper-slide recognition-column">
								<div class="image">
									<a href="https://chambers.com/guide/global?publicationTypeGroupId=2&practiceAreaId=1167&subsectionTypeId=1&locationId=116" target="_blank">
										<img src="img/chambers2.png" alt="">
									</a>
								</div>
								<div class="text sliding-text">
									<h4><a href="https://chambers.com/guide/global?publicationTypeGroupId=2&practiceAreaId=1167&subsectionTypeId=1&locationId=116" target="_blank">Chambers & Partners</a></h4>
									<p>
										The global ranking guide <b><a href="https://chambers.com/guide/global?publicationTypeGroupId=2&practiceAreaId=1167&subsectionTypeId=1&locationId=116" target="_blank">Chambers & Partners ranked Dr. Ilan Cohn</a></b> in Band 1 of its recent rankings of Patent & Trade Mark Agents in Israel. The guide notes that Ilan “is a well-reputed lawyer with notable strength in matters of patent prosecution,” and that he is “an intelligent and well-spoken attorney, and has that presence where a client wants to seek him out for advice." 
									</p>
								</div>
				        	</div>
				        	<!--chambers-->

				        	<!--global leaders-->
				        	<div class="swiper-slide recognition-column">
								<div class="image">
									<a href="https://www.iam-media.com/ilan-cohn" target="_blank"><img src="img/gl.png" alt=""></a>
								</div>
								<div class="text sliding-text">
									<h4><a href="https://www.iam-media.com/ilan-cohn" target="_blank">IAM</a></h4>
									<p>
										In its 2021 issue, the <b><a href="https://www.iam-media.com/ilan-cohn" target="_blank">IAM Strategy 300 Global Leaders guide</a></b> listed Dr. Ilan Cohn as one of the world leaders in the IP sector. IAM noted that "Ilan Cohn has a bright business mind and is gifted at harnessing patents and other rights to address his clients' commercial needs. Strategically savvy and trend aware, he stays on the cutting edge of modern intellectual property development."
									</p>
								</div>
				        	</div>
				        	<!--global leaders-->			        				        	
				        </div>

				    </div>

				     <div class="swiper-pagination rec-pagination"></div>


						  			    <!-- If we need navigation buttons -->
				    <div class="recognition-prev swiper-button-prev">
						<svg xmlns="http://www.w3.org/2000/svg" width="32" height="60" viewBox="0 0 32 60"><g><g><path fill="none" stroke="#707070" stroke-miterlimit="20" d="M30.713 59.427v0L1 29.714v0L30.713 0v0"/></g></g></svg>
				    </div>
				    <div class="recognition-next swiper-button-next">
						<svg xmlns="http://www.w3.org/2000/svg" width="32" height="60" viewBox="0 0 32 60"><g><g><path fill="none" stroke="#707070" stroke-miterlimit="20" d="M1 .001v0l29.713 29.713v0L1 59.428v0"/></g></g></svg>
				    </div>	


</div>


				<div class="read-more">
					<span class="title">READ MORE</span> <span class="arrow"></span>
				</div>
			</div>
		</section>
		<!--recognitions-->



		<!--team-->

		<section class="team" id="team">

			<div class="container">

				<header class="section-header">

					<h2 class="section-title">

						Our Team

					</h2>

				</header>

<!-- 				<div class="main-sub-title">
					Professional Staff
				</div> -->



				<?php include 'team-rows/team_row_1.php';?>

				<?php include 'team-rows/team_row_2.php';?>

				<?php include 'team-rows/team_row_3.php';?>

				<?php include 'team-rows/team_row_4.php';?>

				<?php include 'team-rows/team_row_4_2.php';?>

				<!-- <div class="team-setperator"></div> -->

				<?php //include 'team-rows/team_row_4_3.php';?>

				<div class="team-setperator"></div>

				<?php include 'team-rows/team_row_5.php';?>

				<?php include 'team-rows/team_row_6.php';?>

				<?php include 'team-rows/team_row_6_2.php';?>


				<div class="team-setperator"></div>

<!-- 				<div class="main-sub-title">
					Staff
				</div> -->

				<?php include 'team-rows/team_row_7.php';?>

				<?php include 'team-rows/team_row_8.php';?>

				<div class="team-setperator"></div>

				<?php include 'team-rows/team_row_9.php';?>

			</div>

		</section>

		<!--team-->



		<!--services-->

		<section class="services" id="services">

			<div class="container">

				<header class="section-header">

					<h2 class="section-title">

						Our Services

					</h2>

				</header>



				<div class="parts-wrapper">

					<div class="left-part">

						<p>

                          Cohn, de Vries, Stadler & Co. is a one stop professional service provider to knowledge and IP-based enterprises for building and growing value and revenue-generating IP assets. We combine many decades of expertise of leading IP professionals from a very wide range of IP disciplines to offer a full gamut of IP services, from in-depth analysis of a patent landscape, expert advice on how to better protect knowledge and ideas through to the build-up of high tier value-generating portfolios of patents and other IP assets.

						</p>

					</div>



					<div class="right-part">

						<p>The professional services we provide, include, among others</p>

						<ul class="accordion" id="accordion">



							<li>

								<div class="title">Strategic IP related</div>

								<div class="text">

									<p>

										including devising and implementing IP strategy, IP due diligence and audit, freedom-to-operate (FTO) and patentability studies, consultancy on standard-essential patents, inventions’ harvesting, advisory on M&A and licensing-related IP services

									</p>

								</div>

							</li>		



							<li>

								<div class="title">Patent related</div>

								<div class="text">

									<p>including drafting and prosecuting patent applications, management of patent portfolio, prior art and FTO searches and opinions.</p>

								</div>

							</li>


							<li>

								<div class="title">Designs</div>

								<div class="text">

									<p>

										including managing international designs portfolio, prior art searches, filing and prosecution ,FTO searches and opinions.

									</p>

								</div>

							</li>	


							<li>

								<div class="title">Trademark related</div>

								<div class="text">

									<p>including assistance in branding, trademark clearance searches, filing and prosecuting trademark applications</p>

								</div>

							</li>

							

							<li>

								<div class="title no-arrow">IP litigation support</div>

								<div class="text"></div>

							</li>

							

							<li>

								<div class="title">IP monetization</div>

								<div class="text">

									<p>

										including sale and licensing of patent and other IP assets and enforcement of IP rights

									</p>

								</div>

							</li>


						</ul>

					</div>

				</div>



			</div>

		</section>

		<!--services-->

		<!--articles-->
		<section class="articles" id="articles">
			<div class="container">

				<header class="section-header">

					<h2 class="section-title">

						News

					</h2>

				</header>

				<div class="articles-wrapper">
						<!-- Slider main container -->
						<div class="swiper-container">
						    <!-- Additional required wrapper -->
						    <div class="swiper-wrapper">
						        <!-- Slides -->


					        	<!--webinar-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				<time>August 2021</time>
						        			</div>
						        			</br>
						        			<h3 class="article-title">
						        				IP MANAGERS FORUM WEBINARS
						        			</h3>

						        			<div class="article-body">

						        				<a href="https://www.ipr-resources.com/webinars" target="_blank">
						        				<img src="img/webinar.jpg" alt="Registration on the IPR Website">
						        				</a></br></br>

						        				<p>
						        					<b>Topic:</b> Making or Breaking a Technology Deal:

													The Critical Role of IP Managers<br/>

													<b>Faculty:</b> Dr. Ilan Cohn & Edith Sokol<br/>

													Founding Partners at Cohn, de Vries, Stadler & Co.<br/><br/>

													<b>Wednesday August 4th, 16:00 IST/15:00 CET/14:00 GNT/09:00 EST</b><br/><br/>

													<a href="https://www.ipr-resources.com/webinars" target="_blank">Registration on the IPR Website</a>
						        				</p>	

						        			</div>

						        			<a href="https://www.ipr-resources.com/webinars" class="read-more" target="_blank">Registration on the IPR Website <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--webinar-->


					        	<!--IAM Patent 1000-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				IAM Patent 1000
						        				<time>June 2021</time>
						        			</div>
						        			<h3 class="article-title">
						        				CDS Is ranked as a recommended firm (prosecution category) by IAM Patent 1000 guide 2021.
						        			</h3>

						        			<div class="article-body">


						        				<p>
												We are truly honored! Our firm has been ranked by <a href="https://www.iam-media.com/directories/patent1000/rankings/israel" target="_blank">IAM Patent 1000 guide 2021</a> as a recommended firm (prosecution category), and three founding partners were ranked as recommended individuals : <a href="https://www.iam-media.com/directories/patent1000/individuals/david-de-vries" target="_blank">David de Vries</a>, <a href="https://www.iam-media.com/directories/patent1000/individuals/ilan-cohn" target="_blank">Ilan Cohn</a> and <a href="https://www.iam-media.com/directories/patent1000/individuals/svetlana-stadler" target="_blank">Svetlana Stadler</a>.

						        				</p>
						        				<br/>

						        				<a href="https://www.iam-media.com/directories/patent1000/rankings/israel" target="_blank">
						        				<img src="img/patent.png" alt="IAM Patent 1000 ">
						        				</a>
						        				
						        			</div>

						        			<a href="https://www.iam-media.com/directories/patent1000/rankings/israel" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--IAM Patent 1000-->


					        	<!--Legal 500-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				The Legal 500
						        				<time>May 2021</time>
						        			</div>
						        			<h3 class="article-title">
						        				CDS is ranked as a leading firm in The Legal 500
						        			</h3>

						        			<div class="article-body">


						        				<p>
												We are honored and proud! Our firm has been ranked by the Legal 500 esteemed ranking guide as a leading firm in Israel (Patents Filing and Prosecution category)!

						        				</p><br/><br/>

						        				<img src="img/legal_firm.png" alt="The Legal 500">
						        				
						        			</div>

						        			<a href="https://bit.ly/3tYYSQ3" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--Legal 500-->

					        	<!--Israeli Plastics Magazine-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				PlasticTime - Israeli Plastics Magazine
						        				<time>May 23, 2021</time>
						        			</div>
						        			<h3 class="article-title">
						        				Patents made of Plastic<br/>
						        				(By Ayelet Shwartz)
						        			</h3>

						        			<div class="article-body">
						        				<p>
												The Israeli plastics industry is a major export industry and integrates into every area of our lives. Like any industry that wants to maintain its relevance, it is rich in developments in the invention of products, raw materials, technologies for their processing, and working methods and production. Protecting development rights through a patent is an effective way to maintain a market advantage and prevent legal complications that involve infringing the rights of others, sometimes inadvertently or from a lack of knowledge.
						        				</p>
						        				<p>
												In the attached article published in PlasticTime - Israeli Plastics Magazine , David de Vries, a Founding Partner at our firm, and Yaron Reshef, one of our esteemed clients and the owner of Testa Technologies, a major player in the plastics industry, answer key questions regarding the importance of IP.
						        				</p>
						        			</div>

						        			<a href="https://plastictime.co.il/%d7%96%d7%a8%d7%a7%d7%95%d7%a8-%d7%9c%d7%aa%d7%a2%d7%a9%d7%99%d7%99%d7%94/patent/" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--Israeli Plastics Magazine-->

					        	<!--pc news-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				People & Computers
						        				<time>March 25, 2021</time>
						        			</div>
						        			<h3 class="article-title">
						        				How to protect our inventions?
						        			</h3>

						        			<div class="article-body">
						        				<p>
												Dr. Ilan Cohn, who has three decades of experience in the field of patents, recommends how to conduct, especially when it comes to deals of mergers and acquisitions.
												</br>
												By Yehuda Confortas
						        				</p>
						        			</div>

						        			<a href="https://www.pc.co.il/news/334906/" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--pc news-->


					        	<!--DUNS-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				Dun's100
						        				<!-- <time>March 25, 2021</time> -->
						        			</div>
						        			<h3 class="article-title">
						        				Cohn, de Vries, Stadler & Co
						        			</h3>

						        			<div class="article-body">


						        				<p>
													Cohn, de Vries, Stadler & Co – A Leading Firm in the "Intellectual Property Patent Prosecution" category for 2021</br>

													
														</br>
													We are proud and excited for the privilege of being one of the leading firms in the prestigious Dun's100 rankings in the "Intellectual Property Patent Prosecution" category for 2021!
													A firm like ours, in its first year of operation, that enters the ranking, is a precedent-setting event, for which we owe a debt of gratitude to our loyal clients and wonderful staff. Congratulations to all the others ranked!
						        				</p>

						        				<img src="img/duns.png" alt="Dun's100">
						        				
						        			</div>

						        			<a href="https://www.duns100.co.il/en/Cohn_de_Vries_Stadler_&_Co" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--DUNS-->


					        	<!--ILAN RECOGNITION-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
<!-- 						        			<div class="details">
						        				Maariv
						        				<time>December 28, 2020</time>
						        			</div>
						        			<h3 class="article-title">
						        				Three senior patent attorneys are setting up a new firm
						        			</h3> -->

						        			<div class="article-body">
						        				<p>
													Dr. Ilan Cohn has been recognized as one of the leading patent attorneys in Israel, and was ranked by the international ranking guide Chambers and Partners in the top tier of the Intellectual Property: Patent & Trade Mark Agents practice area.
												</p>
												<p>
													Chambers and Partners noted that Ilan “ is a well-reputed lawyer with notable strength in matters of patent prosecution... He's an intelligent and well-spoken attorney, and has that presence where a client wants to seek him out for advice. He's a very pleasant man as well."
												</p>
												<a href="https://chambers.com/profile/individual/289300?publicationTypeId=2" target="_blank"><img src="https://cms.chambers.com/Logo/4/553/23286679/289300" alt="undefined" border="0" /></a>
						        				
						        			</div>

						        			<a href="https://chambers.com/guide/global?publicationTypeGroupId=2&practiceAreaId=1167&subsectionTypeId=1&locationId=116" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--ILAN RECOGNITION-->


					        	<!--MAARIV-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				Maariv
						        				<time>December 28, 2020</time>
						        			</div>
						        			<h3 class="article-title">
						        				Three senior patent attorneys are setting up a new firm
						        			</h3>

						        			<div class="article-body">
						        				<p>
													A major change in the field of intellectual property in Israel: Three senior partners from the Reinhold Cohn Group, decided to separate and establish a new firm called Cohn, de Vries, Stadler & Co. "We intend to be a significant factor in intellectual property”.
						        				</p>
						        			</div>

						        			<a href="https://www.maariv.co.il/business/economic/israel/Article-811384" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--MAARIV-->


						        <!--JERUSALEM POST-->
						        <div class="swiper-slide">
						        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				The Jerusalem Post
						        				<time>December 30, 2020</time>
						        			</div>
						        			<h3 class="article-title">
						        				Three Senior Patent Attorneys Establish a New Firm:
						        			</h3>

						        			<div class="article-body">
						        				<p>
						        					“Intend to be a Significant Player in the Israeli IP Scene”
						        				</p>
						        			</div>

						        			<a href="https://www.jpost.com/special-content/three-senior-patent-attorneys-establish-a-new-firm-653515" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
						        </div>
						        <!--JERUSALEM POST-->

					        	<!--FORBES-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				Forbes
						        				<time>January 4, 2020</time>
						        			</div>
						        			<h3 class="article-title">
						        				IP-Key Asset of the Modern Economy: What is to be Expected Following the Covid-19 Pandemic (By Ilan Cohn)
						        			</h3>

						        			<div class="article-body">
						        				<p>
						        				Intellectual property (IP) is a key economic driver of the modern economy and is changing its role from a tool for maintaining exclusivity, to one that supports collaboration between stakeholders, each sharing their IP for a common goal.
						        				</p>
						        			</div>

						        			<a href="https://forbes.co.il/e/ip-key-asset-of-the-modern-economy-what-is-to-be-expected-following-the-covid-19-pandemic" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--FORBES-->

					        	<!--Calcalist-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				Calcalist
						        				<time>October 11, 2020</time>
						        			</div>
						        			<h3 class="article-title">
						        				An earthquake in Reinhold Cohn:</br> 3 executives retire to new office
						        			</h3>

						        			<div class="article-body">
						        				<p>
						        				Three patent attorneys, including the grandson of the firm's founder, are retiring from Israel’s leading intellectual property firm due to "ideological differences." More retirees from the firm are expected to join the three.
						        				</p>
						        			</div>

						        			<a href="https://www.calcalist.co.il/local/articles/0,7340,L-3870711,00.html" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--Calcalist-->


					        	<!--WALLA-->
					        	<div class="swiper-slide">
					        	<article>
						        		<div class="inner">
						        			<div class="details">
						        				Walla
						        				<time>December 28, 2020</time>
						        			</div>
						        			<h3 class="article-title">
						        				Patented: three partners, one intellectual property firm
						        			</h3>

						        			<div class="article-body">
						        				<p>
													Dr. Ilan Cohn, David de Vries and Svetlana Stadler have set up new practice with a staff of 25 employees, that handles thousands of patent, design, and trademark cases: "We intend to be a significant factor in the field of intellectual property in Israel”.
						        				</p>
						        			</div>

						        			<a href="https://finance.walla.co.il/item/3408001" class="read-more" target="_blank">READ MORE <i><img src="img/arrow-right.svg" alt=""></i></a>
						        		</div>
						        	</article>
					        	</div>
					        	<!--WALLA-->

						     
						    </div>
						</div>	
			    <!-- If we need navigation buttons -->
			    <div class="swiper-button-prev">
					<svg xmlns="http://www.w3.org/2000/svg" width="32" height="60" viewBox="0 0 32 60"><g><g><path fill="none" stroke="#707070" stroke-miterlimit="20" d="M30.713 59.427v0L1 29.714v0L30.713 0v0"/></g></g></svg>
			    </div>
			    <div class="swiper-button-next">
					<svg xmlns="http://www.w3.org/2000/svg" width="32" height="60" viewBox="0 0 32 60"><g><g><path fill="none" stroke="#707070" stroke-miterlimit="20" d="M1 .001v0l29.713 29.713v0L1 59.428v0"/></g></g></svg>
			    </div>	


			    <div class="swiper-pagination"></div>

				</div>

						
			</div>
		</section>

		<!--articles-->



	
		<?php include 'content-parts/contact-section.php';?>


		<footer style="padding: 0.3rem 0;text-align: center;font-size: 0.6rem;">

			<a href="http://overallstudio.co.il/" rel="nofollow" target="_blank">Overall design</a> | <a href="https://epicod.co.il/" rel="nofollow" target="_blank">Epicod development</a>

		</footer>		

	</main>






<div class="cookie">
	<div class="cookie__inner">
		<p class="cookie__text">This website uses cookies to improve your user experience and to allow you to use certain services and functions.</p>
		<button class="cookie__accept">Accept cookies</button>
		<a class="cookie__policy" href="https://cds-ip.co.il/privacy-policy.php" title="Privacy Policy">Privacy Policy</a>
	</div>
</div>






	<script src="js/jquery-3.5.1.min.js"></script>

	<script src="js/jquery.validate.min.js"></script>

	<script src="js/swiper-bundle.min.js"></script>

	<script src="js/main.js"></script>

<script>(function(){ var s = document.createElement('script'), e = ! document.body ? document.querySelector('head') : document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, language : 'en', position : 'left', leadColor : '#1b1c21', triggerColor : '#ffb32f', triggerRadius : '5px', triggerPositionX : 'left', triggerPositionY : 'bottom', triggerIcon : 'wheels', triggerSize : 'small', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'left', triggerPositionY : 'bottom', triggerOffsetX : 5, triggerOffsetY : 5, triggerRadius : '5px' } }); }; e.appendChild(s);}());</script>

</body>

</html>