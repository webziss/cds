				<div class="team-row">



					<div class="team-wrapper">

						<!--alexsey-->

						<div class="member empty" id="">

							<div class="inner">

								<img src="img/team/Aleksey_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Aleksey_First.jpg')"></div>

								<div class="title">
									<div class="name">Aleksey Belov</div>
									<div class="position">IS&IT Manager</div>
								</div>

								<div class="bg-image hover-img" style="background-image:url('img/team/Aleksey_Second.jpg')"></div>

							</div>


							<!--
							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>DR. HADAS MOR</h3>

										<div class="sub-title">

											Paralegal

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:irenee@cds-ip.co.il" target="_blank" rel="_nofollow">irenee@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/irene-ezratty-farhi-686a5269/" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>LL.M. in International Private Law from the University of Paris (Sorbonne).

									</p> 



	                               <strong>Experience</strong>



	                               <p>Irene is a trademark Lawyer and Notary in Israel. In the last 15 years she has been dealing with branding and trademark registration. She has acquired her extensive experience in the most prominent Israeli law firms in the field of intellectual property until she established her independent practice almost a decade ago.

									</p>



	                               <strong>Services</strong>



	                               <p>Irene handles all aspects related to the management of a trademark portfolio, including brand selection, preliminary searches, trademark registration in Israel and worldwide, and she deals with all events that may occur during the life of a trademark, such as office actions, oppositions, cancellation, assignment or licensing.</p>





	                               <strong>Memberships</strong>



	                               <p>INTA (International Trademark Association)

									Intellectual Property Committee of the Israel Bar Association

									Trademark Consultant at the Israel Export Institute

									</p>



								</div>

							</div>-->

						</div>

						<!--alexsey-->

																								

					</div>



					<!-- <div class="text-wrapper"> -->



					<!-- </div> -->



				</div>