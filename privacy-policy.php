<!doctype html>

<html>

<head>

	<title>Cohn, de Vries, Stadler & Co. - Privacy Policy</title>

	<meta charset="UTF-8">

	<meta name="description" content="Modern and dynamic firm concentrating decades of joint experience in providing high-quality IP services.">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="https://gmpg.org/xfn/11">

	<link rel="stylesheet" href="https://use.typekit.net/wuz0xor.css">

	<link rel="stylesheet" href="css/swiper-bundle.min.css">

	<link rel="stylesheet" href="css/main.css?v=10">





	<link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">

	<link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">

	<link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">

	<link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">

	<link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">

	<link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">

	<link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">

	<link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">

	<link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">

	<link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">

	<link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">

	<link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">

	<link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">

	<link rel="manifest" href="/manifest.json">

	<meta name="msapplication-TileColor" content="#ffffff">

	<meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">

	<meta name="theme-color" content="#ffffff">



<!-- Global site tag (gtag.js) - Google Analytics -->

	<script async src="https://www.googletagmanager.com/gtag/js?id=G-M7CMSPWE2W"></script>

	<script>

	  window.dataLayer = window.dataLayer || [];

	  function gtag(){dataLayer.push(arguments);}

	  gtag('js', new Date());

	 

	  gtag('config', 'G-M7CMSPWE2W');

	</script>

	<!--Reactflow--><script src="https://cdnflow.co/js/5379.js"></script><!--/Reactflow-->





	<!-- <script src="https://unpkg.com/swiper/swiper-bundle.js"></script> -->

	<!-- <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script> -->

</head>







<body id="top" class="simple-page">

	

	<?php include 'content-parts/header.php';?>


	<main class="main-content simple-page">

		


	
		<section class="simple-text">
			<div class="container" style="text-align: justify;">
				<h1>COHN, DE VRIES, STADLER & CO. <br/>PRIVACY POLICY</h1>

				<h2>Introduction</h2>
				<p>
					This Privacy Policy describes the policy of Cohn, de Vries, Stadler & Co. regarding the collection, use  and disclosure of your personal data that you provide when accessing, viewing and/or using our website found at <a href="https://cds-ip.co.il" target="_blank">www.cds-ip.co.il</a> (the "<b>Website</b>"). With this Privacy Notice, we want to make you aware of how we collect and process your personal data as a data controller.
				</p>
				<p>
					By accessing, viewing and/or using our Website, you are agreeing to comply with and be bound by the following Privacy Policy. 
				</p>
				<p>
					If you do not agree to this Privacy Policy at any time, you should not access, view or use our Website.
				</p>
				<p>
					The term "<b>Cohn, de Vries, Stadler & Co.</b>", "<b>us</b>", "<b>we</b>" or "<b>our</b>", refers to Cohn, de Vries, Stadler & Co., the owner of the Website.
				</p>
				<p>
					The term "<b>you</b>", "<b>your</b>" and "<b>yours</b>" refers to any person or entity accessing or using the Website.
				</p>
				<p>
					The term "<b>personal data</b>" means any information relating to you which allows us or any third party to identify you, whether directly or indirectly.
				</p>
				<p>
					This Privacy Policy forms an integral part of our Terms of Use, and the provisions thereof, including (without limitation) the provisions regarding limitation on liability, governing law and dispute resolution, apply to this Privacy Policy.
				</p>
				<p>
					Unless otherwise stated, any defined terms used herein shall have the meaning assigned to them in the Terms of Use.
				</p>


				<h2>Your consent</h2>

				<p>By accessing, viewing and/or using the Website, you consent to: 
					<ul>
						<li>The collection, processing, manipulation, storage, transfer, disclosure, sharing and other uses of your personal data as described in the Terms of Use and in this Privacy Policy; </li>
						<li>To the transfer of such personal data outside of the country where you reside in and outside the U.S.A. and the European Economic Area ("EEA") and other regions with comprehensive data protection laws, to other countries worldwide for storage, processing and use by us, and to the transmission of data on an international basis; and</li>
						<li>The placing of cookies on your computer or internet-enabled device, as described below.</li>
					</ul>

				</p>

				<h2>What personal data do we collect?</h2>
				<p>
					We may collect the following categories of personal data:
					<ul>
						<li><b>Log data: </b>Our servers automatically record information created by your access, viewing and use of the Website and sent to us by your computer, mobile phone or other access device. Such data is collected by server logs and cookies (see explanation below), and may include information such as IP address and login information, device ID or unique identifier, device type, geo-location information, operating system and browser type, the date and time of visits to the Website, the pages viewed or searched for, time spent at our Website, and the Internet sites visited just before and just after our Website.</li>
						<li>
							<b>Our services: </b>We collect personal data and information concerning a person for the purposes of assessing whether we may enter into a professional relationship with that person, and subsequently in the course of that professional relationship. Such personal data may include your full name; contact information; your image; personal data provided to us by you or generated by us in the course of providing legal services to you; and any other information you chose to share with us. <br/><br/>
							You may also give your consent for the processing of your personal data for one or more specific purposes.
						</li>
						<li>
							<b>Contacting us: </b>If you contact us through the Website, email or otherwise contact us, we may keep your message, email address and contact information to respond to your request. Providing such information by you is entirely optional.
						</li>
					</ul>
				</p>

				<h2>Cookies and site tracking</h2>
				<p>
					We use cookies and similar technologies (e.g., web beacons, pixels, ad tags and device identifiers) to recognize you and/or your device(s) on, off and across different services and devices. 
				</p>
				<p>
					A cookie is a small text file, which often includes an anonymous unique identifier. When you visit an Internet site, that site's computer asks your computer for permission to store this file in a part of your hard drive specifically designated for cookies. Each Internet site can send its own cookie to your browser if your browser's preferences allow it, but, to protect your privacy, your browser only permits an Internet site to access the cookies it has already sent to you, not the cookies sent to you by other sites. 
				</p>
				<p>
					Most web browsers automatically accept cookies, but, if you wish, you can change these browser settings by accepting, rejecting and deleting cookies. The "help" portion of the toolbar on most browsers will tell you how to prevent your browser from accepting new cookies, how to have the browser notify you when you receive a new cookie, or how to disable cookies altogether. If you choose to change these settings, you may find that certain functions and features will not work as intended.
				</p>
				<p>
					As you use our Website, the Website uses its cookies to differentiate you from other Users. Cookies, in conjunction with our Internet server's log files, allow us to calculate the aggregate number of people visiting our Website and which parts of the Website are most popular. 
				</p>
				<p>
					In addition, this Website uses the following services: 
					<ul>
						<li>
							<u>Google Analytics.</u> Google Analytics is a web analysis service provided by Google Inc., which works using cookies. This helps us gather feedback in order to constantly improve our Website and better serve our customers. Information about cookies used by or in connection with Google Analytics can be found at: <br/>

							<u><a href="https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage">https://developers.google.com/analytics/devguides/collection/analyticsjs/cookie-usage.</a></u>
						</li>
						<li>
							<u>Conversion rate optimization (CRO tools).</u> CROs collect data about our Website and its visitors to help us understand how people use our Website with the use of cookies.
						</li>
						<li>
							<u>Global site tag.</u> The global site tag sets new cookies on our domain that will store a unique identifier for a user, or for the ad click that brought the user to the Website. The cookies receive the ad click information from a GCLID ("Google Click Identifier") parameter, that Google Ads adds to the landing page URL just before redirecting to the Website. 
						</li>
					</ul><br/>
					By accessing, viewing and/or using our Website, you agree to the placement of cookies on your computer in accordance with the terms of this Privacy Policy.
				</p>

				<h2>What do we use your personal data for, why and for how long?</h2>

				<p>
					<u>Purposes for collecting and processing your personal data</u></br>
					We use the information you provide primarily for the provision of legal services.</br>
					In addition, we may use personal data we collect for the following purposes:
					<ul>
						<li>Compliance with legal and regulatory obligations;</li>
						<li>Facilitating the access, viewing and use of the Website by you;</li>
						<li>Contacting you in the event it becomes necessary to do so;</li>
						<li>Protecting our rights and/or the rights of third parties.</li>
						<li>Sending you, from time to time, marketing communications regarding our services. <br/><br/>
							You will have the right to opt in or to opt out of such marketing communications. You will also be given the opportunity on every marketing email that we send you to indicate that you no longer wish to receive our direct marketing material.

						</li>
					</ul><br/><br/>

					<u>The legal basis for collecting and processing your personal data</u><br/>
					We shall only process your personal data where we have a legal basis to do so. The legal basis will depend on the reasons we process and store your personal data for.<br/>
					Such legal basis may be one or more of the following:

					<ul>
						<li>You have given your consent to us processing your personal data (e.g., as set out in the Section 'Your consent' above);</li>
						<li>Processing is necessary for the performance of the contract between us; </li>
						<li>Processing is necessary for compliance with a legal obligation which applies to us; and/or</li>
						<li>Processing is necessary to protect your vital interests or those of another person; and/or</li>
						<li>Processing is necessary for the purposes of the legitimate interests pursued by us or by a third party.</li>
					</ul><br/><br/>

					<u>Aggregate Insights</u><br/>
					We use your information to produce aggregate insights that do not identify you. For example, we may use your data to generate statistics about our users, their profession or industry, the number of ad impressions served or clicked on, or the demographic distribution of visitors to the Website.<br/><br/>

					<u>Retaining your personal data</u><br/>
					We retain your personal data in accordance with applicable law and our document retention policy. We will not retain your information for longer than is required or necessary. 
				</p>


				<h2>Sharing your personal data</h2>

				<p>
					We may share your personal data with the following third parties and/or in the following circumstances:
					<ul>
						<li>Service providers we are using to run our business, such as cloud services providers who support our business processes and/or store our data, located in multiple locations world-wide;</li>
						<li>Government authorities, law enforcement bodies and regulators for compliance with legal obligations which apply to us;</li>
						<li>Legal and other professional advisers, law courts and law enforcement bodies in all countries we operate in, in order to enforce our legal rights in relation to our contract with you.</li>
						<li>If we are involved in a merger, acquisition, reorganization or sale of assets, or in bankruptcy: Your personal data may be sold or transferred as part of that transaction. In any such transfer of personal data, we shall ensure the confidentiality of any personal data involved in such transactions and provide notice before personal data is transferred and becomes subject to a different privacy policy.</li>
					</ul>
				</p>

				<h2>Security of your personal data</h2>

				<ul>
					<li>We implement what we believe to be appropriate security measures in the storage and disclosure of your personal data, to safeguard it against accidental loss, use or access in an unauthorized way.</li>
					<li>However, we cannot warrant the security of any information that you send us. There is no guarantee that data may not be accessed, used, disclosed, altered, or destroyed by breach of any of our physical or technical safeguards. You hereby acknowledge and agree that your submission of such information is at your sole risk, and we hereby disclaim any and all liability for any disclosure, loss or liability relating to such information in any way.</li>
					<li>We do not have any control over what happens between your device and the boundary of our information infrastructure. You should be aware of the many information security risks that exist and take appropriate steps to safeguard your own information.</li>
				</ul>

				<h2>Your data protection rights</h2>

				<p>
					Under certain circumstances and only to the extent such rights are provided to you by applicable by law, you have the following rights:<br/>
					<ul>
						<li><b>Right to be informed: </b>You have a right to request information about whether we hold personal data about you, and, if so, what that data is and why we are holding/using it.</li>
						<li>
							<b>Right of access: </b> You have a right to request access to your personal data (commonly known as a "data subject access request"). This enables you to receive a copy of the personal data we hold about you and to check that we are lawfully processing it.
						</li>
						<li>
							<b>Right of rectification: </b> You have a right to request rectification of the personal data that we hold about you. This enables you to have any incomplete or inaccurate information we hold about you corrected. 
						</li>
						<li>
							<b>Right to erasure: </b> You have a right to request erasure of your personal data. This enables you to ask us to delete or remove personal data where there is no good reason for us continuing to process it. You also have the right to ask us to delete or remove your personal data where you have exercised your right to object to processing (see below). 
						</li>
						<li>
							<b>Right to restriction of processing: </b> You have a right to request the restriction of processing of your personal data. This enables you to ask us to suspend the processing of personal data about you, for example if you want us to establish its accuracy or the reason for processing it.
						</li>
						<li><b>Right to data potability: </b>You have a right to request the transfer of your personal data in an electronic and structured form to you or to another party (commonly known as a right to 'data portability'). This enables you to take your data from us in an electronically useable format and to be able to transfer your data to another party in an electronically useable format.</li>
						<li><b>Right to object:</b>You have a right to object to processing of your personal data where we are relying on a legitimate interest (or that of a third party) and there is something about your particular situation which makes you want to object to processing on this ground. You also have the right to object where we are processing your personal data for direct marketing purposes. <br/><br/>You can object to our processing of your data for direct marketing purposes by unsubscribing from our mailing list (see Section 'What do we use your personal data for, why and for how long?' for more details).</li>
					</ul>
				</p>

				<p>
					If you want to exercise any of these rights, you can contact us as mentioned in Section 'Contact information' below.
				</p>
				<p>
					You will not have to pay a fee to access your personal data (or to exercise any of the other rights). However, we may charge a reasonable fee if your request for access is clearly unfounded or excessive. Alternatively, we may refuse to comply with the request in such circumstances.
				</p>
				<p>This Privacy Policy should help you to better understand how we use your personal data, it explains in detail the types of personal data we collect, what we use it for and who we may share it with. If you have any further questions about this Privacy Policy or how we handle your personal data, which are not dealt with here, please contact us as mentioned in Section 'Contact information' below.
				</p>

				<h2>External websites </h2>

				<p>We are not responsible or liable for the practices employed by external websites linked to or from our Website nor the information or content contained therein. When you use a link to go from our Website to an external website, our Privacy Policy is no longer in effect. Your browsing and interaction on any external websites, including Internet sites which have a link on our Website, is subject to that external website's own rules and policies. </p>

				<h2>GDPR Point of Contact</h2>

				<p>
					We have appointed GDPR points of contact for your convenience: <br/>
					Primary: 	<a href="mailto:saalon@cds-ip.co.il" target="_blank">saalon@cds-ip.co.il</a> (Designated GDPR Officer).<br/>
					Secondary: 	<a href="mailto:irism@cds-ip.co.il" target="_blank">irism@cds-ip.co.il</a> (Escalation point).
				</p>

				<h2>Contact information</h2>
				<p>
					If you have any questions or comments about this Privacy Policy, please contact us at <a href="mailto:info@cds-ip.co.il" target="_blank">info@cds-ip.co.il</a>.
				</p>
				<p>
					In addition, you always have the right to make a complaint at any time to a supervisory authority. The German Data Protection Commissioner is the lead data protection supervisory authority for Cohn, de Vries, Stadler & Co.
				</p>

				<h2>Changes to this Privacy Policy</h2>

				<p>
					We may change this Privacy Policy at any time and from time to time without liability or notice. Any such change will be effective as of the date it is posted on this page. 
				</p>
				<p>
					Your continued use of the Website after the effective date of any changes to this Privacy Policy, will be deemed to constitute your acceptance of any and all such changes.
				</p>
				<p>
					This Privacy Policy was last updated on August 9, 2021.
				</p>

			</div>
		</section>








		<?php include 'content-parts/contact-section.php';?>



		<footer style="padding: 0.3rem 0;text-align: center;font-size: 0.6rem;">

			<a href="http://overallstudio.co.il/" rel="nofollow" target="_blank">Overall design</a> | <a href="https://epicod.co.il/" rel="nofollow" target="_blank">Epicod development</a>

		</footer>		

	</main>













	<script src="js/jquery-3.5.1.min.js"></script>

	<script src="js/jquery.validate.min.js"></script>

	<script src="js/swiper-bundle.min.js"></script>

	<script src="js/main.js"></script>

<script>(function(){ var s = document.createElement('script'), e = ! document.body ? document.querySelector('head') : document.body; s.src = 'https://acsbapp.com/apps/app/dist/js/app.js'; s.async = true; s.onload = function(){ acsbJS.init({ statementLink : '', footerHtml : '', hideMobile : false, hideTrigger : false, language : 'en', position : 'left', leadColor : '#1b1c21', triggerColor : '#ffb32f', triggerRadius : '5px', triggerPositionX : 'left', triggerPositionY : 'bottom', triggerIcon : 'wheels', triggerSize : 'small', triggerOffsetX : 20, triggerOffsetY : 20, mobile : { triggerSize : 'small', triggerPositionX : 'left', triggerPositionY : 'bottom', triggerOffsetX : 5, triggerOffsetY : 5, triggerRadius : '5px' } }); }; e.appendChild(s);}());</script>

</body>

</html>