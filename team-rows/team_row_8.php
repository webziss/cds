				<div class="team-row">



					<div class="team-wrapper">



						<!--eli-->

						<div class="member empty" id="tm11">

							<div class="inner">

								<img src="img/team/Eli_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Eli_First.jpg')"></div>


								<div class="title">
									<div class="name">ELI PETER</div>
									<div class="position">Maintenance & logistics</div>
								</div>


								<div class="bg-image hover-img" style="background-image:url('img/team/Eli_Second.jpg')"></div>

							</div>


							<!--
							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>Erin Sherf</h3>

										<div class="sub-title">

											Advocate

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:Erins@cds-ip.co.il" target="_blank" rel="_nofollow">Erins@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/miritlotan1" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>LL.B., College of Law and Business, 2017</br>

									  Certified mediator since 2014

									</p> 



	                               <strong>Experience</strong>



	                               <p>Erin has been practicing law since 2016. Before dealing in IP law, she handled administrative law and performed legal proceedings, negotiation and a wide range of general legal counselling through her work with local authorities and public organizations. 

									</p>

									<p>

										In her current position Erin mainly consults and handles filing and prosecution of industrial design applications for local and international clients, in Israel and abroad.

									</p>



	                               <strong>Services</strong>



	                               <p>Designs, Trademarks </p>





	                               <strong>Industries</strong>



	                               <p>Consumer Products, Packaging (PackTech), Automotive, Aviation, Aerospace & Defense, Telecommunications & Media, Medical Devices</p>



	                               <strong>Memberships</strong>



	                               <p>Israel Bar Association</p>



								</div>

							</div>-->

						</div>

						<!--ayelet-->



						<!--Dina-->

						<!-- <div class="member empty" id="tm12"> -->

							<!-- <div class="inner"> -->

								<!-- <img src="img/team/Dina_First.jpg" alt=""> -->

								<!-- <div class="bg-image" style="background-image:url('img/team/Dina_First.jpg')"></div> -->

								<!-- <div class="title"> -->
									<!-- <div class="name">DINA ELIASOOF</div> -->
									<!-- <div class="position">Co-Office Manager</div> -->
								<!-- </div> -->

								<!-- <div class="bg-image hover-img" style="background-image:url('img/team/Dina_Second.jpg')"></div> -->

							<!-- </div> -->


							<!--
							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>MIMI PERETZ</h3>

										<div class="sub-title">

											Senior Paralegal

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:Liorc@cds-ip.co.il" target="_blank" rel="_nofollow">Liorc@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/lior-cohn-4a7959120/" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>B.Sc., Physics @ Biology, 2015, Tel Aviv University, Tel Aviv

									</p> 



	                               <strong>Experience</strong>



	                               <p>Lior is experienced in drafting patent applications and managing patent prosecution procedures in a variety of technological fields, including medical devices, optics, MEMS-based technologies, energy generation production systems, water desalination technologies, water purification systems, mechanics, sensors and RF-based systems. Lior practices complex due diligence studies that include patentability analyses and freedom to operate analyses.

									</p>



	                               <strong>Services</strong>



	                               <p>Patent Portfolio Management, IP Strategic Counseling, IP Due Diligence, Patentability Studies, Freedom to Operate Studies.</p>





	                               <strong>Industries</strong>



	                               <p>Medical Devices, Aviation, Aerospace & Defense, Water Technologies, Optics, MEMS</p>



	                               <strong>Memberships</strong>



	                               <p>Association of Israel Patent Attorneys,  AIPPI</p>



								</div>

							</div>-->

						<!-- </div> -->

						<!--rony-->



						<!--miri-->

<!-- 						<div class="member empty" id="tm12">

							<div class="inner">

								<img src="img/team/Miri_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Miri_First.jpg')"></div>

								<div class="title">
									<div class="name">MIRI SHADLEZKI</div>
									<div class="position">Co-Office Manager</div>
								</div>

								<div class="bg-image hover-img" style="background-image:url('img/team/Miri_Second.jpg')"></div>

							</div> -->


							<!--
							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>DR. HADAS MOR</h3>

										<div class="sub-title">

											Paralegal

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:irenee@cds-ip.co.il" target="_blank" rel="_nofollow">irenee@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/irene-ezratty-farhi-686a5269/" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>LL.M. in International Private Law from the University of Paris (Sorbonne).

									</p> 



	                               <strong>Experience</strong>



	                               <p>Irene is a trademark Lawyer and Notary in Israel. In the last 15 years she has been dealing with branding and trademark registration. She has acquired her extensive experience in the most prominent Israeli law firms in the field of intellectual property until she established her independent practice almost a decade ago.

									</p>



	                               <strong>Services</strong>



	                               <p>Irene handles all aspects related to the management of a trademark portfolio, including brand selection, preliminary searches, trademark registration in Israel and worldwide, and she deals with all events that may occur during the life of a trademark, such as office actions, oppositions, cancellation, assignment or licensing.</p>





	                               <strong>Memberships</strong>



	                               <p>INTA (International Trademark Association)

									Intellectual Property Committee of the Israel Bar Association

									Trademark Consultant at the Israel Export Institute

									</p>



								</div>

							</div>-->

						<!-- </div> -->

						<!--miri-->

																								

					</div>



					<!-- <div class="text-wrapper"> -->



					<!-- </div> -->



				</div>