				<div class="team-row">



					<div class="team-wrapper">



						<!--ilan-->

						<div class="member" id="tm1">

							<div class="inner">

								<img src="img/team/Ilan_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Ilan_First.jpg')"></div>

								<div class="title">
									<div class="name">Dr. Ilan Cohn</div>
									<div class="position">Founding Partner, Patent Attorney</div>
								</div>

								<div class="bg-image hover-img" style="background-image:url('img/team/Ilan_Second.jpg')"></div>

							</div>



							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>Dr. Ilan Cohn</h3>

										<div class="sub-title">

											Founding Partner</br>

											Patent Attorney

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:Ilanc@cds-ip.co.il" target="_blank" rel="_nofollow">Ilanc@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/ilancohn/" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>Ph.D. Biology, 1988, Hebrew University of Jerusalem.</p> 



	                               <strong>Experience</strong>



	                               <p>Ilan is a highly experienced professional in a vast range of IP areas. Among his expertise, gained in 35 years of professional practice, is IP strategy and turning IP rights into first tier, value-generating assets. He advises many large, medium and small enterprises in IP matters, assisting and often leading business development and IP transaction processes. He is widely known for his expertise and received top tier rankings in all local and international professional guides.</p>



	                               <strong>Professional Expertise</strong>



	                               <p>IP Strategic Counseling, IP Transactions & Licensing, IP Due Diligence, Patents, Trademarks, Designs, Plant Breeders’ Rights</p>



	                               <strong>Technologies</strong>



	                               <p>Agritech, Foodtech, Life Science, Healthcare & Digital Health, Medical Devices, Pharmaceuticals, Chemical, Cleantech & Renewables, Consumer Products, Manufacturing, Water Technologies</p>



	                               <strong>Memberships</strong>



	                               <p>AIPPI, LES, FICPI, INTA, Israel Patent Attorneys Association</p>

	                               <strong>Recognition</strong>

	                               <p>
	                               	 Ilan has been recognized as one of the leading patent attorneys in Israel, and was ranked by the international ranking guide <b>Chambers and Partners</b> in the top tier of the Intellectual Property: Patent & Trade Mark Agents practice area.
	                               </p>
	                               <p>
	                               	Chambers and Partners noted that Ilan “ is a well-reputed lawyer with notable strength in matters of patent prosecution... He's an intelligent and well-spoken attorney, and has that presence where a client wants to seek him out for advice. He's a very pleasant man as well."</br></br>
									<a href=https://chambers.com/profile/individual/289300?publicationTypeId=2 target="_blank">
										<img src="/img/Recognition1.png" alt="undefined" border="0" />
									</a>           	
	                               </p>
	                               <p>
									Ilan has been recognized as one of the leading patent attorneys in Israel, and was ranked by the international ranking guide <b>Legal 500:</b>  leading individuals: intellectual property: filing and prosecution: patents<br/></br>
									<a href="https://www.legal500.com/firms/235784-cohn-de-vries-stadler-co/235200-tel-aviv-israel/" target="_blank">
										<img src="/img/Recognition2.png" alt="The Legal 500 – The Clients Guide to Law Firms" border="0">
									</a>
	                               </p>

<!-- 	                               <div class="flexible-row">
	                               	<div class="item">

	                               	</div>
	                               	<div class="item">

	                               	</div>
	                               </div> -->
								</div>

							</div>





						</div>

						<!--ilan-->



						<!--svetlana-->

						<div class="member" id="tm2">

							<div class="inner">

								<img src="img/team/Svetlana_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Svetlana_First.jpg')"></div>

								<div class="title">
									<div class="name">Svetlana Stadler</div>
									<div class="position">Founding Partner, Patent Attorney</div>
								</div>

								<div class="bg-image hover-img" style="background-image:url('img/team/Svetlana_Second.jpg')"></div>

							</div>



							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>Svetlana Stadler</h3>

										<div class="sub-title">

											Founding Partner</br>

											Patent Attorney

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:Svetlanas@cds-ip.co.il" target="_blank" rel="_nofollow">Svetlanas@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/svetlana-stadler-%D7%A1%D7%91%D7%98%D7%9C%D7%A0%D7%94-%D7%A9%D7%98%D7%93%D7%9C%D7%A8-07b3b8202/" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>M.Sc. Physics, 1984, University of Kishinev, Moldova<br>

									Central College of Patent Studies, 1987, Moscow, Russia

									</p> 



	                               <strong>Experience</strong>



	                               <p>Svetlana has over 25 years of experience in management of the IP matters of companies including portfolio management, strategic counseling, due diligence, opinions regarding patentability and FTO issues, patent invalidation and oppositions, as well as drafting and prosecuting patent applications in a vast range of physics areas, and search strategies and search results analysis. Svetlana works for start-up companies, entrepreneurs, IP companies of higher education institutions and venture capital funds, large corporations and multi-international companies.</p>



	                               <strong>Professional Expertise</strong>



	                               <p>IP Strategic Counseling, IP Due Diligence, Patent filing and prosecution, IP Portfolio Management</p>



	                               <strong>Technologies</strong>



	                               <p>Electronic devices, Semiconductor materials and devices, Optics, Electro-Optics, Medical Devices, Energy production, Laser systems, Nanotechnology, Imaging, Digital printing, Communication, Computer Science</p>



	                               <strong>Memberships</strong>



	                               <p>Israel Patent Attorneys Association, AIPPI</p>



								</div>

							</div>

						</div>

						<!--svetlana-->



						<!--david-->

						<div class="member" id="tm3">

							<div class="inner">

								<img src="img/team/David_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/David_First.jpg')"></div>

								<div class="title">
									<div class="name">David De Vries</div>
									<div class="position">Founding Partner, Patent Attorney</div>
								</div>

								<div class="bg-image hover-img" style="background-image:url('img/team/David_Second.jpg')"></div>

							</div>



							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>David De Vries</h3>

										<div class="sub-title">

											Founding Partner</br>

											Patent Attorney

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:David@cds-ip.co.il" target="_blank" rel="_nofollow">David@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/daviddevries1/?originalSubdomain=il" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>B.Sc. Mechanical Engineering, Cum Laude, 1986, Ben-Gurion University

									</p> 



	                               <strong>Experience</strong>



	                               <p>David has vast experience of over 30 years in different IP related fields, including developing IP protection strategies, drafting patent and design applications in a variety of domains, and their  prosecution.  In addition, David is experienced in providing opinions on patentability and freedom to operate issues, and consulting throughout stages of IP decision making.</p>



	                               <strong>Professional Expertise</strong>



	                               <p>Patents, Designs, IP Due Diligence, IP Strategic Counseling.</p>



	                               <strong>Technologies</strong>



	                               <p>Military Technology, Machinery, Engineering, Agritech, Automotive, Aviation, Aerospace & Defense, Cleantech & Renewables, Construction & Engineering, Consumer Products, Foodtech, Manufacturing Technologies, Medical Devices, Packaging (PackTech), Water Technologies, Energy</p>

	                               <strong>Memberships</strong>


	                               <p>AIPPI (Israel Section), FICPI, Israel Patent Attorneys Association</p>


								</div>

							</div>

						</div>

						<!--david-->

																								

					</div>



					<div class="text-wrapper">



					</div>



				</div>