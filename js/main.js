(function ($) {


    $('form').validate({

        rules: {

            // fullname: {

            //   required: true,

            // },      

            // company: {

            //   required: true,

            // },           

            mobile: {

                required: true,

                digits: true

            },

            // email: {

            //   required: true,

            //   email:true,

            // }, 

        },

        messages: {

            mobile: {

                required: 'Required',

                digits: 'Digits only allowed',

            },

        }

    });


    $('.mobile-btn').on('click', function (e) {

        e.preventDefault();

        $('.site-header nav').toggleClass('open');

        $(this).find('.bar').toggleClass('animate');

    });


    function checkHeaderScroll() {

        if ($(this).scrollTop() > ($('.top-banner').height() - $('.site-header').height() * 2)) {

            $('body').addClass('scrolling');

        } else {

            $('body').removeClass('scrolling');

        }

    }

    $(window).scroll(function () {

        checkHeaderScroll();

    });

    checkHeaderScroll();


    var Accordion = function (el, multiple) {

        this.el = el || {};

        this.multiple = multiple || false;

        var links = this.el.find('.title');

        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)

    }


    Accordion.prototype.dropdown = function (e) {

        var $el = e.data.el;

        $this = $(this),

            $next = $this.next();


        $next.slideToggle();

        $this.parent().toggleClass('open');


        if (!e.data.multiple) {

            $el.find('.text').not($next).slideUp().parent().removeClass('open');

        }
        ;

    }


    var accordion = new Accordion($('#accordion'), false);


    $('section#team').on('click', function (e) {

        if ($(e.target).hasClass('team')) {

            $(this).find('.text-wrapper').slideUp(500);

            $(this).find('.team-wrapper .member').removeClass('active');

        }

    });


    var allmembers = $('section.team').find('.member');


    $('.team-row').each(function () {


        var text_area = $(this).find('.text-wrapper');

        var sibling_text_area = $(this).siblings('.team-row').find('.text-wrapper');

        var inner_text = text_area.find('.inner-text');

        var member = $(this).find('.member');


        $.each(member, function (key, el) {


            $(el).click(function () {


                if ($(window).width() > 768) {

                    var id_title = $(this).attr('id');

                    var content = $(this).find('.inner-text').clone();


                    text_area.html(content);


                    allmembers.removeClass('active');

                    $(el).addClass('active');


                    sibling_text_area.slideUp(500);

                    text_area.slideDown(500);

                } else {

                    var content = $(this).find('.inner-text');

                    // allmembers.not(this).removeClass('active');

                    if ($(this).hasClass('active')) {

                        $(this).removeClass('active');

                        content.slideUp(500);

                    } else {

                        $(this).addClass('active');

                        content.slideDown(500);

                    }


                }


            });


        });

        // console.log(member);


    });


    jQuery('a[href^="#"]').on('click', function (e) {

        e.preventDefault();

        e.stopImmediatePropagation();

        // jQuery(document).off("scroll");

        jQuery('.mobile-btn .bar').removeClass('animate');

        jQuery('.site-header nav').removeClass('open');

        jQuery('nav li a').removeClass('active');

        jQuery(this).addClass('active');


        var windowHeight = jQuery(window).height();

        var target = this.hash,

            menu = target;

        $target = jQuery(target);


        offset = $target.offset().top;


        jQuery('html, body').stop().animate({

            'scrollTop': offset - $('.site-header').height()

        }, 500, 'swing', function () {


        });


        jQuery(document).off("scroll");

    });


    function onScroll(event) {

        var scrollPos = jQuery(document).scrollTop();

        var windowHeight = jQuery(window).height();


        jQuery('.site-header li a').each(function () {

            var target = this.hash,

                menu = target;


            $target = jQuery(target);


            // console.log($target);


            if ($target) {


                // if ($target.height() < windowHeight) {

                // offset = $target.offset().top - ($('.site-header').height());

                // }

                // else {

                offset = $target.offset().top - ($('.site-header').height() * 2);

                // }


                if (offset <= scrollPos && offset + $target.height() > scrollPos) {

                    // target.addClass('active');

                    jQuery('nav ul li a').removeClass('active');

                    jQuery('a[href^="' + menu + '"]').addClass('active');

                } else {

                    jQuery('a[href^="' + menu + '"]').removeClass('active');

                }

            }

        });


    }


    jQuery(document).on("scroll", onScroll);


    $('#main-form').on('submit', function (e) {

        e.preventDefault();

        jQuery.ajax({

            url: "contact_mail.php",

            data: 'fullname=' + $("#fullname").val() + '&company=' +

                $("#company").val() + '&mobile=' +

                $("#mobile").val() + '&email=' +

                $('#email').val(),

            type: "POST",

            success: function (data) {

                // console.log(data);

                $('#contact').find('.thankyou-message').html(data);

                $('#contact').find('.thankyou-message').addClass('puff-in-center').delay(8000).queue(function () {

                    $(this).fadeOut(500);

                    $(this).dequeue();

                });

                // $("#mail-status").html(data);

            },

            error: function () {
            }

        });

    })


    var articles_swiper = new Swiper('.articles .swiper-container', {
        // Optional parameters

        slidesPerView: 1,

        // Navigation arrows
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },

        // If we need pagination
        pagination: {
            el: '.swiper-pagination',
        },

        breakpoints: {
            // when window width is >= 480px
            769: {
                slidesPerView: 3,
                // pagination::null
            },
        }

    })


    var recognition_swiper = new Swiper('.recognitions .swiper-container', {
        // Optional parameters

        slidesPerView: 1,
        watchOverflow: true,
        // spaceBetween: 40,

        // If we need pagination
        pagination: {
            el: '.rec-pagination',
            clickable: true
        },

          // Navigation arrows
          navigation: {
            nextEl: '.recognition-next',
            prevEl: '.recognition-prev',
          },

        breakpoints: {
            // when window width is >= 769px
            769: {
                slidesPerView: 4,
                // pagination::null
            },
        }

    });


    $('.recognitions').on('click', '.read-more', function () {
        $(this).toggleClass('close');
        $(this).find('.title').text('CLOSE');
        $('.recognition-column .sliding-text').slideToggle(500);
    });

    $('.open-rights-text').hover(function () {
        $('.rights-text').stop(true, true).slideDown(500);
    });

    /* Cookie */
    function setCookie(name, value, expires) {
        var d = new Date();
        d.setTime(d.getTime() + (expires * 24 * 60 * 60 * 1000));
        var period = "expires=" + d.toUTCString();

        var updatedCookie = name + '=' + encodeURIComponent(value);

        document.cookie = updatedCookie + '; ' + period + '; path=/';
    }

    function getCookie(name) {
        let matches = document.cookie.match(
            new RegExp(
                '(?:^|; )' +
                name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') +
                '=([^;]*)'
            )
        );
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    function text(url) {
        return fetch(url).then((res) => res.text());
    }

    var cookie = document.querySelector('.cookie');
    if (cookie) {
        var location;
        text('https://www.cloudflare.com/cdn-cgi/trace')
            .then(function(data){
                let ipRegex = /[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}/;
                let ip = data.match(ipRegex)[0];
                location = ip;
                return ip;
            })
            .then(function(ip) {
                if (getCookie('cp-accept') === ip) {
                    cookie.classList.contains('visible') && cookie.classList.remove('visible');
                } else {
                    !cookie.classList.contains('visible') && cookie.classList.add('visible');
                }
            });

        var cookieAccept = document.querySelector('.cookie__accept');
        cookieAccept.addEventListener('click', function () {
                setCookie('cp-accept', location, 365);
                cookie.classList.contains('visible') && cookie.classList.remove('visible');
            },
            { passive: true }
        );
    }

})(jQuery);  

