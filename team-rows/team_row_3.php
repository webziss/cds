				<div class="team-row">



					<div class="team-wrapper">



						<!--zvi-->

						<div class="member" id="tm7">

							<div class="inner">

								<img src="img/team/Zvi_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Zvi_First.jpg')"></div>

								<div class="title">
									<div class="name">Zvi Jacobson</div>
									<div class="position">Partner, Patent Attorney</div>
								</div>

								<div class="bg-image hover-img" style="background-image:url('img/team/Zvi_Second.jpg')"></div>

							</div>



							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>Zvi Jacobson</h3>

										<div class="sub-title">
											Partner</br>
											Patent Attorney
										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:Zvij@cds-ip.co.il" target="_blank" rel="_nofollow">Zvij@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/zvi-jacobson-33b8ab9/" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>B.Sc., Electrical and Computer Engineering, 1997, Ben-Gurion University, Beer-Sheva

									</p> 



	                               <strong>Experience</strong>



	                               <p>Zvi is an experienced patent attorney and has been handling IP matters in Israel and worldwide for more than 2 decades. Zvi practices patent searches, patentability opinions, drafting and prosecuting patent applications in electrical engineering, electronics, software engineering, mechanics, optics, medical devices, and cryptosystems.

									</p>



	                               <strong>Professional Expertise</strong>



	                               <p>Patents</p>



	                               <strong>Technologies</strong>



	                               <p>Electrical Engineering, Medical Devices, Electronic Devices, Renewable Energy, Electro-Optics, Imaging, Optics</p>



	                               <strong>Memberships</strong>



	                               <p>Israel Patent Attorneys Association,  AIPPI</p>



								</div>

							</div>

						</div>

						<!--zvi-->



						<!--saar-->

						<div class="member" id="tm8">

							<div class="inner">

								<img src="img/team/Sahar_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Sahar_First.jpg')"></div>

								<div class="title">
									<div class="name">Sa’ar Alon</div>
									<div class="position">Founding Partner, Advocate</div>
								</div>

								<div class="bg-image hover-img" style="background-image:url('img/team/Sahar_Second.jpg')"></div>

							</div>



							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>Sa’ar Alon</h3>

										<div class="sub-title">

											Founding Partner</br>

											Advocate

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:Saara@cds-ip.co.il" target="_blank" rel="_nofollow">Saara@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/saaralon/" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>MBA, 2014, Bar Ilan University</br>

									LL.B., 2005, College of Management Academic Studies</br>

									Licensed mediator since 2004



									</p> 



	                               <strong>Experience</strong>



	                               <p>Sa’ar’s practices include product and brand protection strategic counseling, representation of local and International clients in the area of industrial design protection, legal services related to the filing and prosecution of design applications in Israel and abroad, and additional aspects of intellectual property.  Sa'ar is a member of AIPPI Standing Committee on Designs and INTA’s Non-Traditional Marks Committee and a former member of INTA's Design Committee.

									</p>



	                               <strong>Professional Expertise</strong>



	                               <p>IP Strategic Counseling, Product and Brand Protection, Industrial Designs, Trademarks. </p>





	                               <strong>Memberships</strong>



	                               <p>Israel Bar Association, AIPPI, INTA</p>



								</div>

							</div>

						</div>

						<!--saar-->



						<!--mirit-->

						<div class="member" id="tm9">

							<div class="inner">

								<img src="img/team/Mirit_First.jpg" alt="">

								<div class="bg-image" style="background-image:url('img/team/Mirit_First.jpg')"></div>


								<div class="title">
									<div class="name">Dr. Mirit Lotan</div>
									<div class="position">Founding Partner, Patent Attorney</div>
								</div>


								<div class="bg-image hover-img" style="background-image:url('img/team/Mirit_Second.jpg')"></div>

							</div>



							<div class="inner-text">

								<div class="left-text">

									<div class="tm-name">

										<h3>Dr. Mirit Lotan</h3>

										<div class="sub-title">

											Founding Partner</br>

											Patent Attorney

										</div>

									</div>



									<div class="tm-contact">

										<strong>Contact Info</strong></br>

										T +972 – (0)73-3989000<br/>

									    <a href="mailto:Miritl@cds-ip.co.il" target="_blank" rel="_nofollow">Miritl@cds-ip.co.il</a></br>

									    <ul class="socials">

									    	<li>

									    		<a href="https://www.linkedin.com/in/miritlotan1" target="_blank">

									    			<span class="linkedin"></span>

									    		</a>

									    	</li>

									    </ul>

									</div>

								</div>

								<div class="right-text">

									<strong>Education</strong>



	                               <p>Ph.D. Biology, 1994, The Weizmann Institute of Science



									</p> 



	                               <strong>Experience</strong>



	                               <p>Mirit is a highly knowledgeable professional with 25 years of experience in IP, including 10 years within the biotech industry combining IP with licensing and business development. 

									Mirit is experienced in diverse fields of basic biology and biotechnology, including molecular biology, drug development, diagnostics, therapeutic antibodies, vaccines and more.

									Mirit's clients include multi-national corporations, academia and research institutes, as well as start-up companies. Mirit's expertise ranges from patent drafting and prosecution, patentability and freedom to operate analyses, and strategic consultation on IP matters.



									</p>



	                               <strong>Professional Expertise</strong>



	                               <p>Patents, IP Strategic Counseling, IP Due Diligence </p>





	                               <strong>Technologies</strong>



	                               <p>Life sciences, Pharmaceuticals, Digital Health, Medical Devices, Agritech</p>



	                               <strong>Memberships</strong>



	                               <p>Israel Patent Attorneys Association, AIPPI</p>



								</div>

							</div>

						</div>

						<!--mirit-->

																								

					</div>



					<div class="text-wrapper">



					</div>



				</div>